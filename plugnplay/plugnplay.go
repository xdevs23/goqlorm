// Package plugnplay allows easy and combined configuration
// of the GraphQL + ORM stack with a lot of options
package plugnplay

import (
	"log"
	"net/http"
	"os"

	"gitlab.com/xdevs23/goqlorm/database/dbdefs"

	"github.com/joomcode/errorx"

	"github.com/graphql-go/graphql/gqlerrors"
	"github.com/graphql-go/handler"
	"gitlab.com/xdevs23/goqlorm/api"

	"github.com/graphql-go/graphql"

	"gitlab.com/xdevs23/goqlorm/generator"

	"gitlab.com/xdevs23/goqlorm/database/dbsystem"

	"gitlab.com/xdevs23/goqlorm/database"

	// Import with side-effects to trigger auto registration
	_ "gitlab.com/xdevs23/goqlorm/database/dbsystem/autoregister"
)

type Config struct {
	// Default connection string for connecting to the
	// database. This is used if the environment variable
	// DB_ADDR is not found.
	DefaultConnectionString string

	// Database system to use
	DBSystem dbsystem.DBSystem

	// Database user
	DBUser string

	// Database password
	DBPassword string

	// Database name
	DBName string

	// DBInsertOnMissingEntryToUpdate is used to determine
	// whether to insert when an object to be updated is not found.
	// This is useful when you want to use updates to both insert and
	// update depending on whether the entry exists or not
	DBInsertOnMissingEntryToUpdate bool

	// API handler configuration
	ApiHandlerConfig api.HandlerConfig

	// List of models to use for the database
	// and the GraphQL API.
	// You can either create a new instance of each
	// model and pass that instance or just cast (nil)
	// to a pointer of your model, e. g.:
	//
	//     Models: []interface{}{(*Customer)(nil), (*Product)(nil), (*Order)(nil)}
	//
	Models []interface{}

	// List of models to exclude from the use
	// in the GraphQL API. This might be useful in case
	// you have models that you only use on the server side
	// and don't want to be exposed via the API.
	ExcludeModelsFromGQL []interface{}

	// CustomGQLQueries allows you to add your own queries to the schema
	// or overwrite existing ones
	CustomGQLQueries graphql.Fields

	// CustomGQLMutations allows you to add your own mutations to the schema
	// or overwrite existing ones
	CustomGQLMutations graphql.Fields
}

type PluggedConfig struct {
	Config                 *Config
	GraphQLSchema          *graphql.Schema
	GraphQLSchemaGenerator *generator.GraphQLSchemaGenerator
	Database               *database.Database
	error
}

func Plug(config *Config) *PluggedConfig {
	plug := &PluggedConfig{
		Config: config,
	}

	if config.DBSystem == -1 {
		plug.error = errorx.IllegalArgument.New("unknown db system, choose one of %v", dbsystem.Names)
		return plug
	}

	addr := config.DefaultConnectionString
	if host, exists := os.LookupEnv("DB_ADDR"); exists {
		addr = host
	}
	log.Println("Connecting to", config.DBSystem, "on", addr)

	db := database.Plug(config.DBSystem, &dbsystem.DBAdapterConfig{
		DBUser:                       config.DBUser,
		DBPassword:                   config.DBPassword,
		DBName:                       config.DBName,
		FinalConnectionString:        addr,
		InsertOnMissingEntryToUpdate: config.DBInsertOnMissingEntryToUpdate,
	}, dbdefs.SchemaSyncOptions{}, config.Models...)

	plug.Database = db

	err := db.Prepare()
	if err != nil {
		plug.error = errorx.EnsureStackTrace(err)
		return plug
	}

	schema, gen, err := generator.GenerateGraphQLSchema(
		db, config.ExcludeModelsFromGQL, config.CustomGQLQueries, config.CustomGQLMutations, config.Models...)

	plug.error = err
	plug.GraphQLSchema = &schema
	plug.GraphQLSchemaGenerator = gen

	return plug
}

func (config *PluggedConfig) Play() error {
	// In case of an error, propagate it
	if config.error != nil {
		return config.error
	}

	if _, exists := os.LookupEnv("ENABLE_GRAPHIQL"); exists {
		h := handler.New(&handler.Config{
			Schema:   config.GraphQLSchema,
			Pretty:   true,
			GraphiQL: true,
			FormatErrorFn: func(err error) gqlerrors.FormattedError {
				log.Println(err.Error())
				return gqlerrors.FormatError(err)
			},
		})
		http.Handle("/graphql", h)
	}

	config.Config.ApiHandlerConfig.ExtraConfig = config
	http.HandleFunc("/api/v1",
		api.GenerateGraphQLAPIHandler(config.GraphQLSchema, &config.Config.ApiHandlerConfig))

	listenOn := ":8080"
	if envVar, exists := os.LookupEnv("LISTEN_ADDR"); exists {
		listenOn = envVar
	}

	config.error = http.ListenAndServe(listenOn, nil)

	if err := config.Database.Close(); err != nil {
		return errorx.WrapMany(errorx.InternalError, "multiple errors occurred", config.error, err)
	}
	return errorx.EnsureStackTrace(config.error)
}
