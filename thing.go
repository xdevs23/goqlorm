package main

// Thing is a thing
type Thing struct {
	Id                                   int64
	Name                                 string `description:"Name of the thing"`
	Number                               float64
	UserThatHasThisThing                 *User  `pg:"-" goqlorm.relation.type:"one to one" goqlorm.relation.inversedBy:"Thing" goqlorm.generator.required:"false"`
	UsersThatHaveThisAsAnotherThing      []User `pg:"-" goqlorm.relation.type:"one to many" goqlorm.relation.inversedBy:"AnotherThing" goqlorm.generator.required:"false"`
	UsersThatHaveThisAsPartOfTheirThings []User `pg:"-" goqlorm.relation.type:"many to many" goqlorm.relation.inversedBy:"AllTheThings" goqlorm.generator.required:"false"`
}
