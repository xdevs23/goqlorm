FROM alpine:latest

RUN apk update && apk upgrade && apk add go

COPY . /src

RUN cd /src && go build -o main .
RUN mkdir -p /app && cp /src/main /app/server && cp /src/wait-for /app/ && rm -rf /src /root/go

RUN apk del go

ENTRYPOINT /app/wait-for postgres:5432 -- /app/server

