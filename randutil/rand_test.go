package randutil

import (
	"log"
	"testing"
)

func testAnyRandomString(t *testing.T, fn func(int) string) {
	firstString := fn(8)
	log.Println(firstString)
	i := 0
	for ; i < 3; i++ {
		newString := fn(8)
		log.Println(newString)
		if newString != firstString {
			i = 4
			break
		}
	}
	if i == 3 {
		t.Fatalf("produced the same string (%s) 4 times, this is not random enough", firstString)
	}
}

func TestFastRandomB64UString(t *testing.T) {
	testAnyRandomString(t, FastRandomB64UString)
}

func TestRandomB64UString(t *testing.T) {
	testAnyRandomString(t, RandomB64UString)
}
