// Package randutil has various utilities that
// make the use of randomization easier
package randutil

import (
	"bytes"
	"crypto/rand"
	"encoding/base64"
	"encoding/binary"
	"math"
	rand2 "math/rand"
	"time"
)

func RandomBytes(count int) ([]byte, error) {
	b := make([]byte, count)
	_, err := rand.Read(b)

	if err != nil {
		return nil, err
	}

	return b, nil
}

func FastRandomB64UString(count int) string {
	b := make([]byte, count)
	for i := 0; i < count; i++ {
		b[i] = byte(FastRandomInt(0, 255))
	}
	return base64.URLEncoding.EncodeToString(b)[:count]
}

func RandomB64UString(count int) string {
	b, err := RandomBytes(count)
	if err != nil {
		return FastRandomB64UString(count)
	}
	return base64.URLEncoding.EncodeToString(b)[:count]
}

func FastRandomInt(min uint, max uint) uint {
	ri64 := rand2.NewSource(time.Now().UnixNano()).Int63() / 2
	ri := uint(ri64)
	if ri64 > math.MaxUint32 {
		ri = uint(ri64<<32 ^ ri64>>32)
	}
	if ri < min {
		ri = ri + min
	}
	return uint(math.Mod(float64(ri), float64(max)))
}

func RandomInt(min uint, max uint) uint {
	b, err := RandomBytes(8 /* sizeof(float64) */)
	if err != nil {
		return FastRandomInt(min, max)
	}
	b2, err := RandomBytes(1)
	if err != nil {
		return FastRandomInt(min, max)
	}
	randByte := b2[0]

	buf := bytes.NewBuffer(b)
	i := uint64(0)
	iPtr := &i
	if err = binary.Read(buf, binary.BigEndian, iPtr); err != nil {
		return FastRandomInt(min, max)
	}
	// This will generate a number [0.0..1.0]
	iFraction := uint32(i^math.MaxUint32) >> 9 // sign + exponent
	iExponent := uint32((randByte&0b_0111_1110)|0b_0111_1000) << 23
	i32 := iFraction | iExponent
	if err = binary.Write(buf, binary.BigEndian, i32); err != nil {
		return FastRandomInt(min, max)
	}
	f := float32(0.0)
	fPtr := &f
	if err = binary.Read(buf, binary.BigEndian, fPtr); err != nil {
		return FastRandomInt(min, max)
	}
	return min + uint(float32(max-min)*f)
}
