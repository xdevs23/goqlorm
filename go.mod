module gitlab.com/xdevs23/goqlorm

go 1.17

require (
	github.com/go-pg/pg/v9 v9.2.1
	github.com/golang/protobuf v1.5.2 // indirect
	github.com/graphql-go/graphql v0.7.9
	github.com/graphql-go/handler v0.2.3
	github.com/joomcode/errorx v1.0.3
	github.com/klauspost/cpuid/v2 v2.0.9 // indirect
	github.com/mitchellh/mapstructure v1.4.1
	github.com/segmentio/encoding v0.2.19 // indirect
	github.com/stoewer/go-strcase v1.2.0
	github.com/stretchr/testify v1.6.1
	github.com/vmihailenco/tagparser v0.1.2 // indirect
	github.com/xdevs23/dynamic-struct v10.0.6+incompatible
	github.com/xdevs23/go-custom-base32 v1.0.1
	github.com/xdevs23/straf v2.2.5+incompatible
	github.com/xdevs23/structs v1.1.0
	golang.org/x/crypto v0.0.0-20210513164829-c07d793c2f9a // indirect
	golang.org/x/net v0.0.0-20210614182718-04defd469f4e // indirect
	google.golang.org/appengine v1.6.7 // indirect
)

require (
	github.com/codemodus/kace v0.5.1 // indirect
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/go-pg/zerochecker v0.2.0 // indirect
	github.com/jinzhu/inflection v1.0.0 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/vmihailenco/bufpool v0.1.11 // indirect
	github.com/vmihailenco/msgpack/v4 v4.3.12 // indirect
	google.golang.org/protobuf v1.27.1 // indirect
	gopkg.in/yaml.v3 v3.0.0-20200313102051-9f266ea9e77c // indirect
	mellium.im/sasl v0.2.1 // indirect
)
