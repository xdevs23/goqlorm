package database

import (
	"reflect"

	"gitlab.com/xdevs23/goqlorm/database/dbdefs"

	"gitlab.com/xdevs23/goqlorm/database/dbsystem"

	"gitlab.com/xdevs23/goqlorm/runtimeutil"

	"github.com/joomcode/errorx"

	"gitlab.com/xdevs23/goqlorm/reflectutil"

	"github.com/graphql-go/graphql"
)

const (
	ErrorMustBeInTx = "can't do %s, mutations must be run in a transaction"
)

type TypeName = string
type StructFieldName = string
type StructFieldRelationMap map[StructFieldName]*Relation
type TypeRelationsMap map[TypeName]StructFieldRelationMap

type Database struct {
	Adapter           dbdefs.Adapter
	models            []interface{}
	modelTypes        map[string]reflect.Type
	SchemaSyncOptions dbdefs.SchemaSyncOptions
	relations         TypeRelationsMap
	DBSystem          dbsystem.DBSystem
	AdapterConfig     *dbsystem.DBAdapterConfig
}

type Collection struct {
	Name     string
	database *Database
	isInTx   bool
}

type TransactionFunc func(txDb *Collection) error

func (db *Database) Prepare() error {
	var err error
	if !db.Adapter.IsConnected() {
		return errorx.IllegalState.New(
			"could not connect to database or connection has been terminated. " +
				"Make sure the database system is running and reachable!")
	}
	if err = db.Adapter.SyncSchema(db.SchemaSyncOptions, db.models...); err != nil {
		return err
	}
	db.modelTypes = map[string]reflect.Type{}
	db.relations = TypeRelationsMap{}
	for _, model := range db.models {
		modelType := reflectutil.UnwrapType(reflect.TypeOf(model))
		db.modelTypes[modelType.Name()] = modelType
		db.relations[modelType.Name()] = StructFieldRelationMap{}
	}
	for _, model := range db.models {
		if err = db.buildRelationsFromModel(model); err != nil {
			return errorx.EnsureStackTrace(err)
		}
	}
	return nil
}

func (c *Collection) IsInTx() bool {
	return c.isInTx
}

func (db *Database) Close() error {
	return db.Adapter.Close()
}

func (c *Collection) insert(obj interface{}) error {
	if err := c.EnsureInTx(); err != nil {
		return err
	}
	return c.database.Adapter.Insert(c.Name, obj)
}

func (c *Collection) Insert(obj interface{}) error {
	return c.RunInTransaction(func(txC *Collection) (err error) {
		var finishRunners []onFinishRunner
		if err = txC.insert(obj); err != nil {
			return
		}
		resolvedObj := obj
		if err, finishRunners = txC.resolveRelations(resolvedObj, true); err != nil {
			return
		}
		reflect.ValueOf(obj).Elem().Set(reflect.ValueOf(resolvedObj).Elem())
		err = runFinishRunners(finishRunners)
		return
	})
}

func runFinishRunners(finishRunners []onFinishRunner) (err error) {
	for _, runner := range finishRunners {
		if runner == nil {
			continue
		}
		if err = runner(); err != nil {
			return
		}
	}
	return
}

func (c *Collection) EnsureInTx() error {
	if !c.IsInTx() {
		return errorx.IllegalState.New(ErrorMustBeInTx, runtimeutil.CallerFuncName())
	}
	return nil
}

func (c *Collection) updateUnconditionally(obj interface{}) error {
	if err := c.EnsureInTx(); err != nil {
		return err
	}
	return c.database.Adapter.Update(c.Name, obj)
}

func (c *Collection) update(obj interface{}) (err error, insertedInstead bool) {
	if err := c.EnsureInTx(); err != nil {
		return err, false
	}
	// First check if this even exists
	if _, err := c.Select(reflectutil.Clone(obj)); err == nil {
		err = c.database.Adapter.Update(c.Name, obj)
	} else if c.database.AdapterConfig.InsertOnMissingEntryToUpdate {
		// The config tells us to insert it instead of updating if it can't be found
		err = c.database.Adapter.Insert(c.Name, obj)
		insertedInstead = true
	} else {
		err = errorx.IllegalArgument.New("object %v does not exist and hence cannot be updated. "+
			"If you want to insert instead of getting an error, enable InsertOnMissingEntryToUpdate.", obj)
	}
	return
}

func (c *Collection) Update(obj interface{}) error {
	return c.RunInTransaction(func(txC *Collection) (err error) {
		var finishRunners []onFinishRunner
		if err, _ = txC.update(obj); err != nil {
			return
		}
		resolvedObj := obj
		if err, finishRunners = txC.resolveRelations(resolvedObj, true); err != nil {
			return
		}
		reflect.ValueOf(obj).Elem().Set(reflect.ValueOf(resolvedObj).Elem())
		err = runFinishRunners(finishRunners)
		return
	})
}

func (c *Collection) UpdateSpecifiedFields(obj interface{}, fields []reflect.StructField) error {
	finalFields := fields
	return c.RunInTransaction(func(txC *Collection) (err error) {
		var finishRunners []onFinishRunner
		err, insertedInstead := txC.update(obj)
		if err != nil {
			return err
		}
		// In case we are inserting, we want all fields to be handled
		// otherwise we will get relation resolve errors later on
		if insertedInstead {
			finalFields = make([]reflect.StructField, 0)
		}
		resolvedObj := obj
		if err, finishRunners = txC.resolveRelations(resolvedObj, true, finalFields...); err != nil {
			return
		}
		reflect.ValueOf(obj).Elem().Set(reflect.ValueOf(resolvedObj).Elem())
		err = runFinishRunners(finishRunners)
		return
	})
}

func (c *Collection) delete(obj interface{}) error {
	if err := c.EnsureInTx(); err != nil {
		return err
	}
	return c.database.Adapter.Delete(c.Name, obj)
}

func (c *Collection) Delete(obj interface{}) error {
	return c.RunInTransaction(func(txC *Collection) (err error) {
		var finishRunners []onFinishRunner
		if err, finishRunners = txC.deleteAssociationEntries(obj); err != nil {
			return err
		}
		if err = txC.delete(obj); err != nil {
			return err
		}
		err = runFinishRunners(finishRunners)
		return
	})
}

func (c *Collection) DataFromDatabaseUsingResolveParams(
	objArr interface{}, modelType reflect.Type, p graphql.ResolveParams) error {
	if err := c.database.Adapter.DataFromDatabaseUsingResolveParams(objArr, modelType, p); err != nil {
		return err
	}
	objArrVal := reflectutil.UnwrapValue(reflect.ValueOf(objArr))
	for i := 0; i < objArrVal.Len(); i++ {
		obj := reflectutil.WrapValue(objArrVal.Index(i)).Interface()
		if err, _ := c.resolveRelations(obj, false); err != nil {
			return err
		}
		objArrVal.Index(i).Set(reflect.ValueOf(obj).Elem())
	}
	return nil
}

func (c *Collection) SelectSimple(arguments interface{}) (interface{}, error) {
	return c.database.Adapter.Select(c.Name, arguments)
}

func (c *Collection) Select(arguments interface{}) (interface{}, error) {
	obj, err := c.SelectSimple(arguments)
	if err != nil {
		return nil, err
	}
	if obj == nil {
		return obj, nil
	}
	err, _ = c.resolveRelations(reflectutil.EnsureSinglePointer(obj), false)
	if err != nil {
		return nil, err
	}
	return obj, nil
}

func (c *Collection) SelectSimpleInto(arguments interface{}, model interface{}) error {
	return c.database.Adapter.SelectInto(c.Name, arguments, model)
}

func (c *Collection) SelectInto(arguments interface{}, model interface{}) error {
	err := c.SelectSimpleInto(arguments, model)
	if err != nil {
		return err
	}
	if model == nil {
		return nil
	}
	resolvedModel := model
	err, _ = c.resolveRelations(resolvedModel, false)
	if err != nil {
		return err
	}
	reflect.ValueOf(model).Elem().Set(reflect.ValueOf(resolvedModel).Elem())
	return nil
}

func (c *Collection) SelectAdvanced(model interface{}, query *dbdefs.SelectQuery) error {
	err := c.SelectSimpleAdvanced(model, query)
	if err != nil {
		return err
	}
	if model == nil {
		return nil
	}
	resolvedModel := model
	err, _ = c.resolveRelations(reflectutil.EnsureSinglePointer(model), false)
	if err != nil {
		return err
	}
	reflect.ValueOf(model).Elem().Set(reflect.ValueOf(resolvedModel).Elem())
	return nil
}

func (c *Collection) SelectSimpleAdvanced(model interface{}, query *dbdefs.SelectQuery) error {
	return c.database.Adapter.SelectAdvanced(c.Name, model, query)
}

func (c *Collection) Collection(model interface{}) *Collection {
	return &Collection{
		Name:     reflectutil.GetModelName(model),
		database: c.database,
		isInTx:   c.isInTx,
	}
}

func (c *Collection) CustomCollection(name string) *Collection {
	return &Collection{
		Name:     name,
		database: c.database,
		isInTx:   c.isInTx,
	}
}

func (db *Database) Collection(model interface{}) *Collection {
	return &Collection{
		Name:     reflectutil.GetModelName(model),
		database: db,
	}
}

func (db *Database) CustomCollection(name string) *Collection {
	return &Collection{
		Name:     name,
		database: db,
	}
}

func (c *Collection) RunInTransaction(fn TransactionFunc) error {
	db := c.database
	newAdapter := dbsystem.NewAdapter(db.DBSystem, db.AdapterConfig)
	err := newAdapter.RunInTransaction(func(txAdapter dbdefs.MutationAdapter) error {
		tempCol := &Collection{
			Name:   c.Name,
			isInTx: true,
			database: &Database{
				Adapter: dbdefs.MutationAdapterOverlay{
					FullAdapter:     newAdapter,
					MutationAdapter: txAdapter,
				},
				relations:         db.relations,
				modelTypes:        db.modelTypes,
				models:            db.models,
				SchemaSyncOptions: db.SchemaSyncOptions,
			},
		}
		return fn(tempCol)
	})
	_ = newAdapter.Close()
	return err
}

func Plug(
	system dbsystem.DBSystem, config *dbsystem.DBAdapterConfig,
	schemaSyncOptions dbdefs.SchemaSyncOptions, models ...interface{}) *Database {
	return &Database{
		Adapter:           dbsystem.NewAdapter(system, config),
		models:            models,
		SchemaSyncOptions: schemaSyncOptions,
		DBSystem:          system,
		AdapterConfig:     config,
	}
}

func PlugCustom(
	adapter dbdefs.Adapter, config *dbsystem.DBAdapterConfig,
	schemaSyncOptions dbdefs.SchemaSyncOptions, models ...interface{}) *Database {
	return &Database{
		Adapter:           adapter,
		models:            models,
		SchemaSyncOptions: schemaSyncOptions,
		DBSystem:          -1,
		AdapterConfig:     config,
	}
}
