// Package autoregister is solely used to import database systems
// with side-effects. This means that by importing them, they will
// register themselves as database systems and be ready to use.
package autoregister

import (
	// Import with side effects to auto-register database systems
	_ "gitlab.com/xdevs23/goqlorm/memdb/register"
	_ "gitlab.com/xdevs23/goqlorm/postgres/register"
)
