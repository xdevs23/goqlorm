// Package dbsystem revolves around database system management,
// abstraction and registration
package dbsystem

import (
	"gitlab.com/xdevs23/goqlorm/collections"
	"gitlab.com/xdevs23/goqlorm/database/dbdefs"
)

type DBAdapterConfig struct {
	// DBUser is part of credentials for the database connection
	DBUser string

	// DBPassword is part of credentials for the database connection
	DBPassword string

	// DBName is the database name to use, for example: db
	DBName string

	// FinalConnectionString is auto-generated.
	// This will either be DefaultConnectionString or the value of DB_ADDR
	FinalConnectionString string

	// InsertOnMissingEntryToUpdate is used to determine
	// whether to insert when an object to be updated is not found.
	// This is useful when you want to use updates to both insert and
	// update depending on whether the entry exists or not
	InsertOnMissingEntryToUpdate bool
}

var Names []string

var namesMap = map[string]DBSystem{}

type adapterCreateFunc func(config *DBAdapterConfig) dbdefs.Adapter

var dbSystemToDbMap = map[DBSystem]adapterCreateFunc{}

type DBSystem int

func (dbs DBSystem) String() string {
	if Names != nil && int(dbs) < len(Names) {
		return Names[dbs]
	}
	return "unknown/invalid DBSystem " + string(int64(dbs))
}

func FromString(name string) DBSystem {
	if Names == nil || !collections.Include(Names, name) {
		return -1
	}
	return namesMap[name]
}

func Register(name string, createFunc adapterCreateFunc) {
	newDBSystem := DBSystem(len(Names))
	dbSystemToDbMap[newDBSystem] = createFunc
	Names = append(Names, name)
	namesMap[name] = newDBSystem
}

func NewAdapter(system DBSystem, config *DBAdapterConfig) dbdefs.Adapter {
	return dbSystemToDbMap[system](config)
}
