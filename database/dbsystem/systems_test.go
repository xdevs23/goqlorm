package dbsystem

import (
	"fmt"
	"reflect"
	"testing"

	"gitlab.com/xdevs23/goqlorm/randutil"

	"github.com/graphql-go/graphql"
	"gitlab.com/xdevs23/goqlorm/database/dbdefs"
)

type TestAdapter struct {
}

func (t TestAdapter) SyncSchema(options dbdefs.SchemaSyncOptions, models ...interface{}) error {
	panic("implement me")
}

func (t TestAdapter) Close() error {
	panic("implement me")
}

func (t TestAdapter) Select(collectionName string, args interface{}) (interface{}, error) {
	panic("implement me")
}

func (t TestAdapter) SelectInto(collectionName string, args interface{}, model interface{}) error {
	panic("implement me")
}

func (t TestAdapter) SelectAdvanced(collectionName string, model interface{}, query *dbdefs.SelectQuery) error {
	panic("implement me")
}

func (t TestAdapter) CreateCollectionFromModel(name string, model interface{}, ifNotExists bool) error {
	panic("implement me")
}

func (t TestAdapter) Insert(collectionName string, obj interface{}) error {
	panic("implement me")
}

func (t TestAdapter) Update(collectionName string, obj interface{}) error {
	panic("implement me")
}

func (t TestAdapter) Delete(collectionName string, obj interface{}) error {
	panic("implement me")
}

func (t TestAdapter) DataFromDatabaseUsingResolveParams(objArr interface{}, modelType reflect.Type, p graphql.ResolveParams) error {
	panic("implement me")
}

func (t TestAdapter) RunInTransaction(dbdefs.InnerTransactionFunc) error {
	panic("implement me")
}

func (t TestAdapter) IsConnected() bool {
	panic("implement me")
}

const dbSystemName = "TestSQL"

func TestRegister(t *testing.T) {
	// Generate names
	var dbSystemNames []string
	c := randutil.RandomInt(20, 10000)
	for i := uint(0); i < c; i++ {
		dbSystemNames = append(dbSystemNames, dbSystemName+randutil.RandomB64UString(4)+string(i))
	}
	fmt.Printf("testing with %d names\n", len(dbSystemNames))
	for _, name := range dbSystemNames {
		Register(name, func(config *DBAdapterConfig) dbdefs.Adapter {
			return TestAdapter{}
		})
	}
	for _, name := range dbSystemNames {
		if result := FromString(name).String(); result != name {
			t.Fatalf("expected %s but got %s instead", name, result)
		}
	}
}
