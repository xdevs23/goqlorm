// Package relationtype has all the relation types that can be
// used with this ORM
package relationtype

import "gitlab.com/xdevs23/goqlorm/collections"

// Find out what you need by saying each one of the following sentences:
// OneToOne: One <model name> can only have one <linked model> and vice versa
// OneToMany: One <model name> can have many <linked model>s but each <linked model> can only be linked once
// ManyToOne: Many <model name>s can have one <linked model> each
// ManyToMany: Many <model name>s can have many <linked model>s
//
// Another way to find out what you need is by looking at the type:
// Slice: ManyToMany or OneToMany
// Struct: ManyToOne or OneToOne
//
// Or by looking at both models:
// A has 1 B and B has 1 A: OneToOne
// A has n B and B has 1 A: OneToMany
// A has 1 B and B has n A: ManyToOne
// A has n B and B has n A: ManyToMany
//
// In a OneToMany or ManyToOne scenario you can simply
// specify the inverse in the other model.

const (
	// Every *one* country can have only *one* capital and
	// every one capital can only have one country
	// A 1 <----> 1 B
	OneToOne RelationType = iota
	// Every *one* user has *many* addresses (OneToMany) and
	// each one of those addresses can only have one user (ManyToOne)
	// A 1 <----> n B
	// B n <----> 1 B
	// OneToMany is the inverse of ManyToOne
	// The "One" part specifies which of the two models
	// is only allowed to be owned once by the other one
	OneToMany
	// *Many* addresses can have only *one* user each (ManyToOne) and
	// each of those users can have any number of addresses (OneToMany)
	// A n <----> 1 B
	// B 1 <----> n A
	// ManyToOne is the inverse of OneToMany
	ManyToOne

	// *Many* orders can have *many* products and
	// any product can belong to any order
	ManyToMany
)

var RelationTypeNames = []string{
	OneToOne:   "one to one",
	OneToMany:  "one to many",
	ManyToOne:  "many to one",
	ManyToMany: "many to many",
}

var relationTypeNamesRTMap = map[string]RelationType{
	OneToOne.String():   OneToOne,
	OneToMany.String():  OneToMany,
	ManyToOne.String():  ManyToOne,
	ManyToMany.String(): ManyToMany,
}

type RelationType int

func (rt RelationType) String() string {
	if int(rt) < len(RelationTypeNames) {
		return RelationTypeNames[rt]
	}
	return "invalid RelationType"
}

func FromString(rts string) RelationType {
	if !collections.Include(RelationTypeNames, rts) {
		return -1
	}
	return relationTypeNamesRTMap[rts]
}

func (rt RelationType) IsToMany() bool {
	return rt == OneToMany || rt == ManyToMany
}

func (rt RelationType) IsToOne() bool {
	return !rt.IsToMany()
}
