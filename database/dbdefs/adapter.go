package dbdefs

import (
	"reflect"

	"github.com/graphql-go/graphql"
)

func (overlay MutationAdapterOverlay) SyncSchema(options SchemaSyncOptions, models ...interface{}) error {
	return overlay.FullAdapter.SyncSchema(options, models)
}

func (overlay MutationAdapterOverlay) IsConnected() bool {
	return overlay.FullAdapter.IsConnected()
}

func (overlay MutationAdapterOverlay) Close() error {
	return overlay.FullAdapter.Close()
}

func (overlay MutationAdapterOverlay) Select(collectionName string, args interface{}) (interface{}, error) {
	return overlay.FullAdapter.Select(collectionName, args)
}

func (overlay MutationAdapterOverlay) SelectInto(
	collectionName string, args interface{}, model interface{}) error {
	return overlay.FullAdapter.SelectInto(collectionName, args, model)
}

func (overlay MutationAdapterOverlay) SelectAdvanced(
	collectionName string, model interface{}, query *SelectQuery) error {
	return overlay.FullAdapter.SelectAdvanced(collectionName, model, query)
}

func (overlay MutationAdapterOverlay) CreateCollectionFromModel(name string, model interface{}, ifNotExists bool) error {
	return overlay.MutationAdapter.CreateCollectionFromModel(name, model, ifNotExists)
}

func (overlay MutationAdapterOverlay) Insert(cn string, obj interface{}) error {
	return overlay.MutationAdapter.Insert(cn, obj)
}

func (overlay MutationAdapterOverlay) Update(cn string, obj interface{}) error {
	return overlay.MutationAdapter.Update(cn, obj)
}

func (overlay MutationAdapterOverlay) Delete(cn string, obj interface{}) error {
	return overlay.MutationAdapter.Delete(cn, obj)
}

func (overlay MutationAdapterOverlay) DataFromDatabaseUsingResolveParams(objArr interface{}, modelType reflect.Type, p graphql.ResolveParams) error {
	return overlay.FullAdapter.DataFromDatabaseUsingResolveParams(objArr, modelType, p)
}

func (overlay MutationAdapterOverlay) RunInTransaction(fn InnerTransactionFunc) error {
	return overlay.FullAdapter.RunInTransaction(fn)
}
