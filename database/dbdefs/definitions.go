// Package dbdefs has common definitions of types
// that are related to the database abstraction layer
// and database management
package dbdefs

import (
	"reflect"

	"github.com/graphql-go/graphql"
)

type SchemaSyncOptions struct {
	// Whether to remove columns from the database
	// that can't be found in models.
	//
	// *This is a potentially destructive option, only
	// enable if you're absolutely sure you want to remove
	// old columns*
	//
	// Note that this may not have an effect on document
	// or graph databases.
	RemoveOrphanedColumns bool

	// Model names (in case the collections have a different name)
	ModelNames []string
}

type MutationAdapter interface {
	// CreateCollectionFromModel creates a new collection in the database
	// using the provided model as a schema
	CreateCollectionFromModel(name string, model interface{}, ifNotExists bool) error
	// Insert inserts the given obj into the collection denoted by collectionName
	// and writes the new entry into obj. This means after calling this you will
	// be able to use the new Id
	Insert(collectionName string, obj interface{}) error
	// Update updates the given obj in the collection given denoted by collectionName
	// using the primary key as index (usually Id) and writes the result into obj
	Update(collectionName string, obj interface{}) error
	// Delete deletes the given obj using the primary key as index (usually Id)
	Delete(collectionName string, obj interface{}) error
}

// Use this interface to implement a database interface
// This will be used by the abstraction layer
type Adapter interface {
	// SyncSchema creates tables and adds columns just like they are defined in the models
	SyncSchema(options SchemaSyncOptions, models ...interface{}) error
	// Close closes the database connection
	Close() error
	// Select fetches data from the database using the primary key inside args and
	// writes the result into args. The same result is returned as well.
	Select(collectionName string, args interface{}) (interface{}, error)
	// Select fetches data from the database using the primary key inside args and
	// writes the result into model.
	SelectInto(collectionName string, args interface{}, model interface{}) error
	// SelectAdvanced can be used to fetch data in a wide variety of ways including
	// but not limited to where/equals.
	SelectAdvanced(collectionName string, model interface{}, query *SelectQuery) error

	MutationAdapter

	// DataFromDatabaseUsingResolveParams uses the provided resolve parameters p to fetch
	// the requested data from the database and puts the result into objArr
	DataFromDatabaseUsingResolveParams(objArr interface{}, modelType reflect.Type, p graphql.ResolveParams) error
	// RunInTransaction creates a new transaction, does some overlaying to delegate
	// specific operations to the transaction and runs the given function inside of it.
	RunInTransaction(InnerTransactionFunc) error
	// IsConnected returns a bool indicating whether the connection to the database is
	// established and ready to use
	IsConnected() bool
}

// MutationAdapterOverlay overlays the methods of the
// mutation adapter on top of the Adapter ones.
// This is mainly used to provide the abstracted RunInTransaction
// functionality while keeping the database API the same
type MutationAdapterOverlay struct {
	// FullAdapter is the original, full adapter
	FullAdapter Adapter
	// MutationAdapter provides a layer that interfaces with
	// a transaction as opposed to the FullAdapter which doesn't
	// use transactions at all
	MutationAdapter MutationAdapter
}

type ColumnName = string
type ConditionValue = interface{}

// SelectQuery can be used to describe a select query
// regardless of the database system used
type SelectQuery struct {
	// Conditions
	Conditions map[ColumnName]ConditionValue
	/*GreaterThan interface{}
	SmallerThan interface{}*/
	// IgnoreMultiple indicates whether in case of a result set
	// of greater length than 1 only the first result should be returned.
	// If you set this to true, only the first result will be returned.
	// If you set this to false, all results are returned as a slice.
	IgnoreMultiple bool
}

// InnerTransactionFunc will be run inside a database transaction
type InnerTransactionFunc func(MutationAdapter) error
