package database

import (
	"crypto/md5"
	"crypto/sha256"
	"encoding/base32"
	"errors"
	"fmt"
	"log"
	"reflect"
	"sort"
	"strings"

	"gitlab.com/xdevs23/goqlorm/database/dbdefs"

	dynamicstruct "github.com/xdevs23/dynamic-struct"

	"github.com/joomcode/errorx"

	"gitlab.com/xdevs23/goqlorm/reflectutil"

	"github.com/xdevs23/go-custom-base32/custombase32"
	"gitlab.com/xdevs23/goqlorm/database/relationtype"
)

// Tags
const (
	TagPrefix       = "goqlorm.relation."
	TagRelationType = TagPrefix + "type"
	TagLinkTo       = TagPrefix + "linkTo"
	TagLinkFrom     = TagPrefix + "linkFrom"
	TagInversedBy   = TagPrefix + "inversedBy"
)

const (
	AssocFieldPrefix = "Assoc"
)

type association struct {
	// Id is needed for delete and update
	Id int64
	// Linked fields will be added dynamically
}

type Relation struct {
	ModelType               reflect.Type
	Field                   reflect.StructField
	LinkedType              reflect.Type
	LinkedTo                reflect.StructField
	LinkedFrom              reflect.StructField
	Associations            func(txC *Collection) *Collection
	AssociateFrom           reflect.StructField
	AssociateTo             reflect.StructField
	AssociationType         reflect.Type
	Type                    relationtype.RelationType
	InversedBy              *reflect.StructField
	AssociationEntryIdField reflect.StructField
	LinkedPrimaryKeyField   reflect.StructField
}

type onFinishRunner func() error

type RelationSolveContext struct {
	// For all relations

	Relation              *Relation
	ModelCollection       *Collection
	TransactionCollection *Collection
	FieldValue            *reflect.Value
	ModelValue            *reflect.Value
	ForInsert             bool
	OnFinishRunners       []onFinishRunner

	// These are set at a later point in time and only
	// if needed, so be careful when using them

	LinkedToFieldInsideFieldValue *reflect.Value
	LinkedResult                  interface{}
}

type associationCollectionName = string
type extendedAssociation = interface{} // extended association
type performOnRelationFunc func(ctx *RelationSolveContext) error

var allAssociations = map[associationCollectionName]extendedAssociation{}

func (db *Database) buildRelationsFromModel(model interface{}) error {
	modelType := reflectutil.UnwrapType(reflect.TypeOf(model))
	if modelType.Kind() != reflect.Struct {
		return errors.New(fmt.Sprintf("type %s is of kind %s but need struct",
			modelType.Name(), modelType.Kind()))
	}

	for i := 0; i < modelType.NumField(); i++ {
		field := modelType.Field(i)
		fieldStructType := reflectutil.UnwrapType(field.Type)

		if fieldStructType.Kind() == reflect.Slice {
			fieldStructType = fieldStructType.Elem()
		}

		relationTypeString, found := field.Tag.Lookup(TagRelationType)
		if !found {
			continue
		}
		relationType := relationtype.FromString(relationTypeString)
		if int(relationType) == -1 {
			return errors.New(fmt.Sprintf("relation type %s is invalid for %s.%s, choose one of: %s",
				relationTypeString, modelType.Name(), field.Name,
				strings.Join(relationtype.RelationTypeNames, ", ")))
		}

		relationLink := fieldStructType.Name()

		linkedModelType := db.modelTypes[relationLink]
		if linkedModelType == nil {
			return errors.New(fmt.Sprintf("could not find model %s while trying to build relation for %s",
				relationLink, modelType.Name()))
		}

		relation, err := db.buildRelation(field, modelType, relationLink, linkedModelType, relationType)
		if err != nil {
			return errorx.EnsureStackTrace(err)
		}

		db.relations[relation.ModelType.Name()][field.Name] = relation
	}

	return nil
}

func structFieldsForLinks(
	linkedFromStr, linkedToStr string, linkedModelType reflect.Type,
	modelType reflect.Type, relationType relationtype.RelationType, field reflect.StructField,
	relationLink string) (linkedFrom, linkedTo reflect.StructField, err error) {

	var found bool
	if linkedFrom, found = modelType.FieldByName(linkedFromStr); !found {
		err = errors.New(fmt.Sprintf(
			"field %s.%s not found but is needed to create %v relation between %s.%s and %s (%s.%s -> %s.%s)",
			modelType.Name(), linkedFromStr, relationType, modelType.Name(), field.Name, relationLink,
			modelType.Name(), linkedFromStr, relationLink, linkedToStr))
	}
	if linkedTo, found = linkedModelType.FieldByName(linkedToStr); !found {
		err = errors.New(fmt.Sprintf(
			"field %s.%s not found but is needed to create %v relation between %s.%s and %s (%s.%s -> %s.%s)",
			linkedModelType.Name(), linkedToStr, relationType, modelType.Name(), field.Name, relationLink,
			modelType.Name(), linkedFromStr, relationLink, linkedToStr))
	}

	return
}

func (relation *Relation) linkedFromAssocFieldName() string {
	if relation.InversedBy == nil {
		return AssocFieldPrefix + relation.Field.Name + relation.ModelType.Name() + relation.LinkedFrom.Name
	} else {
		return AssocFieldPrefix + relation.ModelType.Name() + relation.LinkedFrom.Name
	}
}

func (relation *Relation) linkedToAssocFieldName() string {
	if relation.InversedBy == nil {
		return AssocFieldPrefix + relation.LinkedType.Name() + relation.LinkedTo.Name
	} else {
		return AssocFieldPrefix + relation.InversedBy.Name + relation.LinkedType.Name() + relation.LinkedTo.Name
	}
}

func (db *Database) buildRelation(
	field reflect.StructField, modelType reflect.Type, relationLink string,
	linkedModelType reflect.Type, relationType relationtype.RelationType) (*Relation, error) {

	var found bool
	var linkedToStr string
	var linkedFromStr string
	var inversedByFieldPtr *reflect.StructField

	linkedIdField, found := linkedModelType.FieldByName("Id")
	if !found {
		return nil, errorx.IllegalFormat.New("need an Id field in %s but none was found", linkedModelType.String())
	}
	if linkedIdField.Type.Kind() != reflect.Int64 {
		return nil, errorx.IllegalFormat.New("Id field of %s must be int64", linkedModelType.String())
	}

	if linkedToStr, found = field.Tag.Lookup(TagLinkFrom); !found {
		linkedToStr = "Id"
	}
	if linkedFromStr, found = field.Tag.Lookup(TagLinkTo); !found {
		linkedFromStr = "Id"
	}
	if inversedBy, found := field.Tag.Lookup(TagInversedBy); found {
		if inversedByField, found := linkedModelType.FieldByName(inversedBy); found {
			inversedByFieldPtr = &inversedByField
		} else {
			return nil, errorx.IllegalArgument.New("field %s.%s specified by %s of %s.%s not found in %s",
				linkedModelType.Name(), inversedBy, TagInversedBy, modelType.Name(), field.Name, linkedModelType.Name())
		}
	}

	linkedFrom, linkedTo, err := structFieldsForLinks(
		linkedFromStr, linkedToStr, linkedModelType, modelType, relationType, field, relationLink)
	if err != nil {
		return nil, errorx.EnsureStackTrace(err)
	}

	relation := &Relation{
		ModelType:             modelType,
		Field:                 field,
		LinkedType:            linkedModelType,
		LinkedTo:              linkedTo,
		LinkedFrom:            linkedFrom,
		Type:                  relationType,
		LinkedPrimaryKeyField: linkedIdField,
	}

	switch {
	case relationType.IsToOne():
		if field.Type.Kind() != reflect.Ptr {
			return nil, errorx.IllegalFormat.New(
				"for a %s relation, field %s.%s has to be a pointer but found %s",
				relationType.String(), relation.ModelType.Name(),
				field.Name, field.Type.Kind().String())
		}
	case relationType.IsToMany():
		if field.Type.Kind() != reflect.Slice {
			return nil, errorx.IllegalFormat.New(
				"for a %s relation, field %s.%s has to be a slice but found %s",
				relationType.String(), relation.ModelType.Name(),
				field.Name, field.Type.Kind().String())
		}
	}

	// Sort the two collections alphabetically so that we only have one
	// table for both sides
	sortedModelNames := []string{relation.ModelType.Name(), relation.LinkedType.Name()}
	sort.Strings(sortedModelNames)
	sortedFieldNames := []string{relation.LinkedFrom.Name, relation.LinkedTo.Name}
	sort.Strings(sortedFieldNames)
	// This string is unique in both directions - meaning that
	// no matter from which direction you are linking from,
	// this string will always be the same.
	uniqueAssocString := fmt.Sprintf("%s<->%s_%s<->%s",
		sortedModelNames[0], sortedModelNames[1], sortedFieldNames[0], sortedFieldNames[1])
	sha256Assoc := sha256.Sum256([]byte(uniqueAssocString))
	md5Assoc := md5.Sum(sha256Assoc[:])
	hashedAssocString := custombase32.Encoder().WithPadding(base32.NoPadding).EncodeToString(md5Assoc[:])
	finalAssocString := "AssocH" + hashedAssocString

	relation.Associations = func(txC *Collection) *Collection {
		return txC.CustomCollection(finalAssocString)
	}

	linkedFromAssocFieldName := relation.linkedFromAssocFieldName()
	linkedToAssocFieldName := relation.linkedToAssocFieldName()

	assoc := allAssociations[finalAssocString]
	if assoc == nil {
		assoc = &association{}
	}
	assoc = dynamicstruct.ExtendStruct(reflectutil.Unwrap(assoc)).
		AddField(linkedFromAssocFieldName, reflectutil.Unwrap(reflectutil.NewFromType(relation.LinkedFrom.Type)), "").
		AddField(linkedToAssocFieldName, reflectutil.Unwrap(reflectutil.NewFromType(relation.LinkedTo.Type)), "").
		BuildWithPkgPath(relation.ModelType.PkgPath()).New()
	assoc = reflectutil.EnsureSinglePointer(assoc)
	allAssociations[finalAssocString] = assoc

	log.Printf("Association table %s for %v: %s.%s -> %s",
		finalAssocString, relation.Type, relation.ModelType.Name(),
		relation.Field.Name, relation.LinkedType.Name())
	if err := db.Adapter.SyncSchema(
		dbdefs.SchemaSyncOptions{
			RemoveOrphanedColumns: false,
			ModelNames:            []string{finalAssocString},
		},
		reflectutil.EnsureSinglePointer(assoc)); err != nil {
		return nil, errorx.EnsureStackTrace(err)
	}

	assocType := reflectutil.UnwrapType(reflect.TypeOf(assoc))
	relation.AssociateFrom, _ = assocType.FieldByName(linkedFromAssocFieldName)
	relation.AssociateTo, _ = assocType.FieldByName(linkedToAssocFieldName)
	relation.AssociationEntryIdField, _ = assocType.FieldByName("Id")
	relation.AssociationType = assocType
	relation.InversedBy = inversedByFieldPtr

	return relation, nil
}

func (c *Collection) resolveRelations(
	model interface{}, isMutation bool, fields ...reflect.StructField) (err error, finishRunners []onFinishRunner) {

	return c.performOnRelations(model, isMutation, func(ctx *RelationSolveContext) error {
		return ctx.resolveRelation()
	}, fields...)
}

func (c *Collection) performOnRelations(
	model interface{}, isMutation bool, perform performOnRelationFunc,
	fields ...reflect.StructField) (err error, finishRunners []onFinishRunner) {

	finishRunners = []onFinishRunner{}
	modelType := reflect.TypeOf(model)
	modelTypeElem := modelType.Elem()

	if modelTypeElem.Kind() != reflect.Struct {
		return
	}

	modelValue := reflect.ValueOf(model)
	relationMap := c.database.relations[modelTypeElem.Name()]

	numField := len(fields)
	usingSpecifiedFields := numField != 0
	if !usingSpecifiedFields {
		numField = modelTypeElem.NumField()
	}

	for i := 0; i < numField; i++ {
		var field reflect.StructField
		if usingSpecifiedFields {
			field = fields[i]
		} else {
			field = modelTypeElem.Field(i)
		}
		var runners []onFinishRunner
		err, runners = c.performOnRelationForField(
			field, modelValue, isMutation, perform, relationMap)
		if err != nil {
			return
		}
		finishRunners = append(finishRunners, runners...)
	}
	return
}

func (c *Collection) performOnRelationForField(
	field reflect.StructField, modelValue reflect.Value, isMutation bool,
	perform performOnRelationFunc, relationMap StructFieldRelationMap) (err error, finishRunners []onFinishRunner) {

	if isMutation && !c.IsInTx() {
		return errorx.IllegalState.New(
			"when doing mutations, performOnRelationForField must be run in a transaction"), nil
	}

	if reflectutil.UnwrapType(field.Type).Kind() == reflect.Struct {
		fieldObj := reflectutil.EnsureSinglePointerValue(modelValue.Elem().FieldByIndex(field.Index))
		if !fieldObj.IsNil() {
			var returnedFinishRunners []onFinishRunner
			if err, returnedFinishRunners =
				c.performOnRelations(fieldObj.Interface(), isMutation, perform); err != nil {
				return
			}
			finishRunners = append(finishRunners, returnedFinishRunners...)
		}
	}

	fieldValue := modelValue.Elem().FieldByIndex(field.Index)
	fieldRelation := relationMap[field.Name]
	if fieldRelation != nil {
		ctx := &RelationSolveContext{
			ModelCollection:       c,
			TransactionCollection: c,
			FieldValue:            &fieldValue,
			ModelValue:            &modelValue,
			ForInsert:             isMutation,
			Relation:              fieldRelation,
			OnFinishRunners:       finishRunners,
		}
		err = perform(ctx)
		if err != nil {
			return
		}
		finishRunners = ctx.OnFinishRunners
	}
	return
}

func (ctx *RelationSolveContext) insertOrUpdateAssocValues(
	fieldValueEntry reflect.Value, assocEntryValues reflect.Value) {

	fieldRelation := ctx.Relation
	modelValue := ctx.ModelValue

	assocEntry := reflectutil.NewFromType(fieldRelation.AssociationType)

	modelTypeIdField := fieldRelation.LinkedFrom
	elemValIdField := fieldRelation.LinkedTo

	ctx.OnFinishRunners = append(ctx.OnFinishRunners, func() error {
		assocEntryVal := reflect.ValueOf(assocEntry).Elem()
		var newAssociateTo reflect.Value
		if fieldValueEntry.Kind() == reflect.Invalid ||
			(fieldValueEntry.Kind() == reflect.Ptr && fieldValueEntry.IsNil()) {
			// This value is nil, set to -1 to say it isn't associated to anything
			newAssociateTo = reflect.ValueOf(int64(-1))
		} else {
			newAssociateTo = reflectutil.UnwrapValue(fieldValueEntry).FieldByIndex(elemValIdField.Index)
			if newAssociateTo.Interface().(int64) == int64(0) {
				// Set to -1 to explicitly say it isn't associated to anything
				newAssociateTo.SetInt(-1)
			}
		}
		assocEntryVal.FieldByIndex(fieldRelation.AssociateTo.Index).Set(newAssociateTo)
		assocEntryVal.FieldByIndex(fieldRelation.AssociateFrom.Index).Set(
			modelValue.Elem().FieldByIndex(modelTypeIdField.Index))

		c := fieldRelation.Associations(ctx.ModelCollection)
		if assocEntryValues.Len() == 0 {
			return c.insert(assocEntry)
		} else {
			// First test if the association we want to update actually exists
			err, hasValidMatchingEntry, index := ctx.hasValidMatchingAssocEntry(assocEntryValues)
			if err != nil {
				return err
			}
			if hasValidMatchingEntry {
				assocEntryVal.FieldByIndex(fieldRelation.AssociationEntryIdField.Index).Set(
					assocEntryValues.Index(index).FieldByIndex(fieldRelation.AssociationEntryIdField.Index))
				// Just to be sure
				reflect.ValueOf(assocEntry).Elem().Set(assocEntryVal)
				return c.updateUnconditionally(assocEntry)
			} else {
				return c.insert(assocEntry)
			}
		}
	})
}

func (ctx *RelationSolveContext) hasValidMatchingAssocEntry(assocEntryVal reflect.Value) (error, bool, int) {
	has := false
	ix := -1
	return ctx.performOnMatchingAssocEntries(assocEntryVal, func(index int, assocEntryFieldToAssoc reflect.Value) (err error, b bool) {
		// This function is only run if there are any matching entries
		has = true
		ix = index
		return nil, true
	}), has, ix
}

func (ctx *RelationSolveContext) resolveRelation() error {

	isMutation := ctx.ForInsert
	fieldValue := ctx.FieldValue
	fieldRelation := ctx.Relation
	modelValue := ctx.ModelValue

	if modelValue.Elem().FieldByIndex(fieldRelation.LinkedFrom.Index).Interface() == int64(-1) {
		// This is an invalid/null-like object that should be ignored
		return nil
	}

	db := ctx.ModelCollection.database

	var err error

	if isMutation && fieldValue.Kind() == reflect.Slice && fieldRelation.Type.IsToMany() {
		// Delete associations for this relation before doing anything else
		err, runners := ctx.ModelCollection.deleteAssociationEntries(ctx.ModelValue.Interface(), fieldRelation.Field)
		if err != nil {
			return err
		}
		if err = runFinishRunners(runners); err != nil {
			return err
		}
	}

	assocEntries, _, err := ctx.ModelCollection.associationEntriesFor(fieldRelation, modelValue)
	if err != nil {
		return err
	}

	assocEntriesVal := reflect.ValueOf(assocEntries)

	if isMutation {
		if fieldValue.Kind() == reflect.Slice {
			// Now iterate through the slice to insert and
			// insert all those entries as well as their associations
			for i := 0; i < fieldValue.Len(); i++ {
				fv := reflectutil.EnsureSinglePointerValue(fieldValue.Index(i))
				_, err = ctx.insertOrUpdateRelation(&fv, db)
				fieldValue.Index(i).Set(fv.Elem())
				if err != nil {
					return err
				}
				ctx.insertOrUpdateAssocValues(fv, assocEntriesVal)
			}
		} else {
			if _, err := ctx.insertOrUpdateRelation(fieldValue, db); err != nil {
				return err
			}
			ctx.insertOrUpdateAssocValues(fieldValue.Elem(), assocEntriesVal)
		}
		return nil
	}

	if assocEntriesVal.Len() == 0 && !isMutation && fieldRelation.Type.IsToOne() && fieldRelation.InversedBy == nil {
		// This only applies to required to-one fields as to-many can also have zero entries
		// Additionally, inversedBy fields can't emit this error since that is bound to their inverse.
		return errorx.IllegalState.New(
			"can't resolve %v relation %s.%s -> %s using association collection %s from %s(%v) to %s, "+
				"association not found",
			// resolve %v relation %s.%s
			fieldRelation.Type, fieldRelation.ModelType.Name(), fieldRelation.Field.Name,
			// -> %s
			fieldRelation.LinkedType.Name(),
			// collection %s
			fieldRelation.Associations(ctx.ModelCollection).Name,
			// from %s(%v)
			fieldRelation.linkedFromAssocFieldName(),
			modelValue.Elem().FieldByIndex(fieldRelation.LinkedFrom.Index).Interface(),
			// to %s
			fieldRelation.linkedToAssocFieldName(),
		)
	} else if !isMutation {
		// We have a result from the association select
		// Note that in case of a to-many relation with no assoc results,
		// an empty slice will be returned. performOnMatchingAssocEntries won't
		// do anything and such the slice stays empty. The length and capacity
		// is determined by the length of assocEntriesVal which is zero if no
		// association was found.
		finalValue := *fieldValue
		if fieldRelation.Type.IsToMany() {
			finalValue = reflect.ValueOf(reflectutil.EnsureSinglePointer(
				reflectutil.NewSliceOfTypeWithSize(reflectutil.PurifyType(fieldValue.Type()),
					assocEntriesVal.Len(), assocEntriesVal.Len())))
			fieldValue.Set(finalValue.Elem())
		}

		// now let's fetch the other side
		err = ctx.performOnMatchingAssocEntries(assocEntriesVal,
			func(index int, assocEntryFieldToAssoc reflect.Value) (_ error, brk bool) {

				// First create a new object that is of the linked type
				remoteObj := reflectutil.NewFromType(fieldRelation.LinkedType)
				remoteObjVal := reflect.ValueOf(remoteObj).Elem()

				// Now assign the Id that we got from the database to the Id of the remote obj
				remoteObjVal.FieldByIndex(fieldRelation.LinkedTo.Index).Set(assocEntryFieldToAssoc)

				// Fetch it (also, we don't want recursion from here on)
				_, err := db.Collection(fieldRelation.LinkedType).SelectSimple(remoteObj)
				if err != nil {
					return errorx.EnsureStackTrace(err), false
				}

				if fieldRelation.Type.IsToOne() {
					finalValue.Set(reflectutil.EnsureSinglePointerValue(remoteObjVal))
					return nil, true
				} else {
					finalValue.Elem().Index(index).Set(remoteObjVal)
				}
				return nil, false
			})

	}

	return nil
}

func (ctx *RelationSolveContext) performOnMatchingAssocEntries(assocEntriesVal reflect.Value,
	fn func(index int, assocEntryFieldToAssoc reflect.Value) (error, bool /* break */)) error {

	fieldValue := ctx.FieldValue
	fieldRelation := ctx.Relation

	for assocEntryIndex := 0; assocEntryIndex < assocEntriesVal.Len(); assocEntryIndex++ {
		assocEntry := assocEntriesVal.Index(assocEntryIndex)
		fieldNameToAssoc := fieldRelation.linkedToAssocFieldName()
		assocEntryFieldToAssoc := assocEntry.FieldByName(fieldNameToAssoc)
		assocEntryFieldToAssocInt := assocEntryFieldToAssoc.Interface().(int64)

		if !assocEntryFieldToAssoc.IsValid() || assocEntryFieldToAssocInt <= int64(0) {
			isToOne := fieldRelation.Type.IsToOne()
			if assocEntryIndex == assocEntriesVal.Len()-1 || (isToOne && assocEntryFieldToAssocInt == int64(-1)) {
				// We didn't find any matching association, set it to null or an empty slice
				if isToOne {
					fieldValue.Set(reflect.New(fieldValue.Type()).Elem())
				} else {
					fieldValue.Set(reflect.ValueOf(reflectutil.EnsureSinglePointer(
						reflectutil.NewSliceOfType(reflectutil.PurifyType(fieldValue.Type())))).Elem())
				}
				break
			}
			// This isn't the one we need, skip it
			continue
		}

		err, brk := fn(assocEntryIndex, assocEntryFieldToAssoc)
		if err != nil {
			return err
		}
		if brk {
			break
		}
	}
	return nil
}

func (ctx *RelationSolveContext) insertOrUpdateRelation(fieldValue *reflect.Value, db *Database) (result interface{}, err error) {

	if fieldValue.Kind() == reflect.Ptr && fieldValue.IsNil() ||
		fieldValue.Elem().FieldByIndex(ctx.Relation.LinkedPrimaryKeyField.Index).Interface() == int64(-1) {
		// Don't insert anything since -1 means null. Also, remove that from the object (set it to null)
		ctx.ModelValue.Elem().FieldByIndex(ctx.Relation.Field.Index).
			Set(reflect.New(ctx.Relation.Field.Type).Elem())
		return fieldValue.Interface(), nil
	}

	resultVal := reflectutil.EnsureSinglePointerValue(*fieldValue)
	original := resultVal.Interface()
	// Clone before use
	result = reflectutil.CloneValue(resultVal).Interface()
	c := ctx.ModelCollection.Collection(result)
	if fieldValue.Elem().FieldByIndex(ctx.Relation.LinkedPrimaryKeyField.Index).Interface().(int64) < 1 {
		err = c.insert(original)
	} else {
		if err = c.SelectSimpleInto(result, result); err != nil {
			// Does not exist, create it first
			err = c.insert(original)
		} else {
			err = c.updateUnconditionally(original)
		}
	}
	if err != nil {
		err = errorx.EnsureStackTrace(err)
		return
	}
	resultVal = reflectutil.EnsureSinglePointerValue(reflect.ValueOf(original))
	fieldValue.Elem().Set(resultVal.Elem())
	result = original
	return
}

func (c *Collection) associationEntriesFor(fieldRelation *Relation, modelValue *reflect.Value,
) (assocEntries interface{}, assocEntriesPtr interface{}, err error) {
	assocEntries = reflectutil.NewSliceOfType(fieldRelation.AssociationType)
	assocEntriesPtr = reflectutil.EnsureSinglePointer(assocEntries)
	linkedFromInt := modelValue.Elem().FieldByIndex(fieldRelation.LinkedFrom.Index).Interface()
	if linkedFromInt == int64(0) {
		// There is no need to continue from here
		return
	}

	err = fieldRelation.Associations(c).SelectSimpleAdvanced(assocEntriesPtr, &dbdefs.SelectQuery{
		Conditions: map[dbdefs.ColumnName]dbdefs.ConditionValue{
			fieldRelation.linkedFromAssocFieldName(): linkedFromInt,
		},
		IgnoreMultiple: false,
	})
	assocEntries = reflect.ValueOf(assocEntriesPtr).Elem().Interface()
	return
}

func (ctx *RelationSolveContext) resolveToOne() error {
	// TODO: reimplement error checking and remove this function afterwards
	// TODO: this function isn't used, remove it after doing the TODO above
	db := ctx.ModelCollection.database
	isMutation := ctx.ForInsert
	fieldRelation := ctx.Relation
	linkedResult := ctx.LinkedResult
	modelValue := *ctx.ModelValue

	// Looks like this already exists. Check if anyone owns it.
	fieldObj := reflectutil.AutoPointerNewFromValue(modelValue)
	fieldObjVal := reflectutil.UnwrapValue(reflect.ValueOf(fieldObj))
	linkedToField := reflect.ValueOf(linkedResult).Elem().FieldByIndex(fieldRelation.LinkedTo.Index)
	err := db.Collection(fieldRelation.ModelType).SelectSimpleAdvanced(fieldObj, &dbdefs.SelectQuery{
		Conditions: map[dbdefs.ColumnName]dbdefs.ConditionValue{
			fieldRelation.ModelType.FieldByIndex(fieldRelation.LinkedFrom.Index).Name: linkedToField.Interface(),
		},
		IgnoreMultiple: true,
	})
	if err != nil {
		if isMutation {
			// It isn't owned by anything, so just use that
			return nil
		} else {
			return err
		}
	} else if !isMutation {
		return nil
	}

	firstField := fieldObjVal.Type().Field(0)

	// one-to-one means there can only be one link
	// between the two nodes as each of both sides can only
	// own one of the other at the same time
	return errorx.RejectedOperation.New(
		"cannot link %s.%s to %s as %s: %s with %s %v already exists and is linked from %s with %s %v. "+
			"There can only be one link "+
			"between the two nodes as each of both sides can only own one of the other at the same time. "+
			"If you want to allow linking multiple %ss using %s.%s then consider using the %v relation "+
			"type. Otherwise make sure you remove the %s with %s %v before using it for the new %s",

		// cannot link %s.%s
		fieldRelation.ModelType.Name(), fieldRelation.Field.Name,
		// to %s as %s
		fieldRelation.LinkedType.Name(), fieldRelation.Type.String(),
		// from %s with %s %v
		fieldRelation.LinkedType.Name(), fieldRelation.LinkedTo.Name, linkedToField.Interface(),
		// linked from %s
		fieldRelation.ModelType.Name(),
		// with %s %v
		firstField.Name, fieldObjVal.FieldByIndex(firstField.Index).Interface(),
		// multiple %ss using %s.%s
		fieldRelation.LinkedType.Name(), fieldRelation.ModelType.Name(), fieldRelation.LinkedFrom.Name,
		// consider using the %v relation [...] remove the %s with %s ...
		relationtype.OneToMany, fieldRelation.LinkedType.Name(), fieldRelation.LinkedTo.Name,
		// ... %v before [...] the new %s
		ctx.LinkedToFieldInsideFieldValue.Interface(), fieldRelation.ModelType.Name())
}

func (db *Database) IsRelation(model interface{}, field reflect.StructField) bool {
	return db.IsTypeRelation(reflect.TypeOf(model), field)
}

func (db *Database) IsTypeRelation(modelType reflect.Type, field reflect.StructField) bool {
	if relations := db.relations[modelType.Name()]; relations != nil {
		if rel := relations[field.Name]; rel != nil {
			return true
		}
	}
	return false
}

func (c *Collection) deleteAssociationEntries(obj interface{}, fields ...reflect.StructField) (error, []onFinishRunner) {
	return c.performOnRelations(reflectutil.EnsureSinglePointer(obj), false, func(ctx *RelationSolveContext) error {
		assocEntries, _, err := c.associationEntriesFor(ctx.Relation, ctx.ModelValue)
		assocEntriesVal := reflectutil.UnwrapValue(reflect.ValueOf(assocEntries))
		for assocEntryIndex := 0; assocEntryIndex < assocEntriesVal.Len(); assocEntryIndex++ {
			assocEntryVal := assocEntriesVal.Index(assocEntryIndex)
			fieldNameToAssoc := ctx.Relation.linkedToAssocFieldName()
			assocEntryFieldToAssoc := assocEntryVal.FieldByName(fieldNameToAssoc)

			if !assocEntryFieldToAssoc.IsValid() || assocEntryFieldToAssoc.Interface().(int64) < 1 {
				isToOne := ctx.Relation.Type.IsToOne()
				if assocEntryIndex == assocEntriesVal.Len()-1 || isToOne {
					// We didn't find it, so don't do anything
					break
				}
				// This isn't the one we need, skip it
				continue
			}
			if len(fields) > 0 {
				found := false
				for _, field := range fields {
					if ctx.Relation.Field.Name == field.Name {
						found = true
						break
					}
				}
				if !found {
					// This one isn't supposed to be deleted, skip it
					continue
				}
			}
			// Delete the association
			err = ctx.Relation.Associations(c).delete(
				reflectutil.EnsureSinglePointerValue(assocEntryVal).Interface())
		}
		return err
	})
}
