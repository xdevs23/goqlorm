// Package reflectutil contains various utilities
// that are useful when working with reflection
package reflectutil

import (
	"reflect"
	"runtime"

	"github.com/mitchellh/mapstructure"
)

// GetModelName returns the struct name of `model`
// whereas model can be either an instance of a struct
// or reflect.Type. This function also does some additional
// checks and unwrapping to account for pointers, slices, etc.
func GetModelName(model interface{}) string {
	var ty reflect.Type
	switch model.(type) {
	case reflect.Type:
		ty = model.(reflect.Type)
	default:
		ty = reflect.TypeOf(model)
	}
	tyName := ty.Name()
	for ; len(tyName) == 0; ty = ty.Elem() {
		tyName = ty.Elem().Name()
	}
	if ty.Kind() == reflect.Slice || ty.Kind() == reflect.Array {
		tyName = "[]" + tyName
	}
	return tyName
}

// NewFromType creates a new instance of the given type
// and returns a pointer to the new instance
func NewFromType(modelType reflect.Type) interface{} {
	ptr := NewPtrValueFromType(modelType)
	obj := ptr.Interface()
	return obj
}

// NewPtrValueFromType creates a new instance of the given type
// and returns a reflect.Value of a pointer to the new instance
func NewPtrValueFromType(modelType reflect.Type) reflect.Value {
	return reflect.New(modelType)
}

// NewFromModel creates a new instance of the same type as `model`
// and returns a pointer to the new instance
func NewFromModel(model interface{}) interface{} {
	return NewFromType(reflect.TypeOf(model))
}

// SetSlice decodes all elements of `valueSlice` and puts them
// into a new slice which is then assigned to `fieldValue`.
// The expected `valueSlice` slice type is map[string]<anything>
func SetSlice(valueSlice []interface{}, fieldValue reflect.Value) {
	newSlice := reflect.MakeSlice(fieldValue.Type(), len(valueSlice), len(valueSlice))
	for i, val := range valueSlice {
		if reflect.TypeOf(val).Kind() == reflect.Map &&
			fieldValue.Type().Elem().Kind() == reflect.Struct {
			realVal := NewFromType(fieldValue.Type().Elem())
			mapstructure.Decode(val, realVal)
			if newSlice.Index(i).Kind() == reflect.Ptr {
				newSlice.Index(i).Set(reflect.ValueOf(realVal))
			} else {
				newSlice.Index(i).Set(reflect.ValueOf(realVal).Elem())
			}
		} else {
			newSlice.Index(i).Set(reflect.ValueOf(val))
		}
	}
	fieldValue.Set(newSlice)
}

// UnwrapType dereferences all pointers of `typ` and returns
// the actual dereferenced type
func UnwrapType(typ reflect.Type) (unwrapped reflect.Type) {
	unwrapped = typ
	for unwrapped.Kind() == reflect.Ptr {
		unwrapped = unwrapped.Elem()
	}
	return
}

// UnwrapValue dereferences all pointers of `val` and returns
// the actual dereferenced value
func UnwrapValue(val reflect.Value) (unwrapped reflect.Value) {
	unwrapped = val
	for unwrapped.Kind() == reflect.Ptr {
		unwrapped = unwrapped.Elem()
	}
	return
}

// WrapValue wraps `val` into a new pointer. This is equivalent to
// prefixing a non-generic (non-interface{}) variable with an ampersand
func WrapValue(val reflect.Value) reflect.Value {
	p := reflect.New(val.Type())
	p.Elem().Set(val)

	return p
}

// Wrap wraps `obj` into a new pointer. This is equivalent to
// prefixing a non-generic (non-interface{}) variable with an ampersand.
// Use this when you have an interface{} that needs to be a pointer
func Wrap(obj interface{}) interface{} {
	return WrapValue(reflect.ValueOf(obj)).Interface()
}

// Unwrap dereferences all pointers of `obj` and returns
// the actual dereferenced value
func Unwrap(obj interface{}) interface{} {
	val := reflect.ValueOf(obj)
	if val.Kind() != reflect.Ptr {
		return obj
	}
	return val.Elem().Interface()
}

// EnsureSinglePointer makes sure that `obj` is a pointer
// and points to a value. If `obj` is not a pointer, it will
// be wrapped into one. If what `obj` points to is also a pointer,
// it will be unwrapped until it points to a value.
func EnsureSinglePointer(obj interface{}) interface{} {
	return EnsureSinglePointerValue(reflect.ValueOf(obj)).Interface()
}

// EnsureSinglePointerValue makes sure that the kind of `val` is a pointer
// and points to a value. If the kind of `val` is not a pointer, it will
// be wrapped into one. If the element kind of `val` is also a pointer,
// it will be unwrapped until its element's kind is a value.
func EnsureSinglePointerValue(val reflect.Value) reflect.Value {
	if val.Kind() != reflect.Ptr {
		return WrapValue(val)
	}
	for val.Kind() == reflect.Ptr && val.Elem().Kind() == reflect.Ptr {
		val = val.Elem()
	}
	return val
}

// EnsureSinglePointerValue makes sure that the kind of `typ` is a pointer
// and points to a value. If the kind of `typ` is not a pointer, it will
// be wrapped into one. If the element kind of `typ` is also a pointer,
// it will be unwrapped until its element's kind is a value.
func EnsureSinglePointerType(typ reflect.Type) reflect.Type {
	if typ.Kind() != reflect.Ptr {
		return reflect.PtrTo(typ)
	}
	for typ.Kind() == reflect.Ptr && typ.Elem().Kind() == reflect.Ptr {
		typ = typ.Elem()
	}
	return typ
}

// AutoPointerNewFromType creates a new instance of `typ` and makes sure
// that the instance is a pointer and that it points to the new value (and
// not another pointer)
func AutoPointerNewFromType(typ reflect.Type) interface{} {
	return NewFromType(EnsureSinglePointerType(typ))
}

// AutoPointerNewFromType creates a new instance of the type of `val` and makes sure
// that the instance is a pointer and that it points to the new value (and
// not another pointer)
func AutoPointerNewFromValue(val reflect.Value) interface{} {
	return NewPtrValueFromType(EnsureSinglePointerType(val.Type()).Elem()).Interface()
}

// NewSlice creates a new slice of capacity 1 that has the same type as `model`
func NewSlice(model interface{}) interface{} {
	return NewSliceWithSize(model, 0, 1)
}

// NewSliceOfType creates a new slice of capacity 1 of type `typ`
func NewSliceOfType(typ reflect.Type) interface{} {
	return NewSliceOfTypeWithSize(typ, 0, 1)
}

// NewSliceOfTypeWithSize creates a new slice of length `len` and capacity `cap`
// of type `typ`. `cap` should be equal or greater than `len`.
func NewSliceOfTypeWithSize(typ reflect.Type, len int, cap int) interface{} {
	return UnwrapValue(reflect.MakeSlice(reflect.SliceOf(typ), len, cap)).Interface()
}

// NewSliceWithSize creates a new slice of length `len` and capacity `cap`
// that has the same type as `model`. `cap` should be equal or greater than `len`.
func NewSliceWithSize(model interface{}, len int, cap int) interface{} {
	return NewSliceOfTypeWithSize(EnsureSinglePointerType(reflect.TypeOf(model)).Elem(), len, cap)
}

// PurifyType dereferences all pointers, slices, maps, channels and arrays,
// effectively returning the pure type at the end of the chain. This
// means that the type `**[]*string` will be purified to `string` only.
func PurifyType(typ reflect.Type) reflect.Type {
	// According to reflect.Type.Elem() documentation
	for typ.Kind() == reflect.Array ||
		typ.Kind() == reflect.Chan ||
		typ.Kind() == reflect.Map ||
		typ.Kind() == reflect.Ptr ||
		typ.Kind() == reflect.Slice {

		typ = typ.Elem()
	}
	return typ
}

// PurifyType dereferences all pointers, slices, maps, channels and arrays,
// effectively returning the pure type at the end of the chain. This
// means that the type `**[]*string` will be purified to `string` only.
func PurifyToType(model interface{}) reflect.Type {
	return PurifyType(reflect.TypeOf(model))
}

// FuncNameOf returns the name of the function `fn`
func FuncNameOf(fn interface{}) string {
	v := reflect.ValueOf(fn)
	if rf := runtime.FuncForPC(v.Pointer()); rf != nil {
		return rf.Name()
	}
	return v.String()
}

// NameOf returns the name of whatever is passed as `anything`
func NameOf(anything interface{}) (str string) {
	t := reflect.TypeOf(anything)

	// In case this panics, trap it and return
	// a string representation of the reflect type
	defer func() {
		recover()
		str = t.String()
	}()

	switch t.Kind() {
	case reflect.Func:
		str = FuncNameOf(anything)
	default:
		str = PurifyType(t).String()
	}
	return
}

func Clone(obj interface{}) interface{} {
	return CloneValue(reflect.ValueOf(obj)).Interface()
}

func CloneValue(val reflect.Value) reflect.Value {
	return EnsureSinglePointerValue(UnwrapValue(val))
}

func GetFieldByName(obj interface{}, name string) interface{} {
	val := UnwrapValue(reflect.ValueOf(obj))
	typ := UnwrapType(reflect.TypeOf(obj))
	field, found := typ.FieldByName(name)
	if !found {
		return nil
	}
	return val.FieldByIndex(field.Index).Interface()
}

func SetFieldByName(obj interface{}, name string, fieldObj interface{}) bool {
	val := UnwrapValue(reflect.ValueOf(obj))
	typ := UnwrapType(reflect.TypeOf(obj))
	field, found := typ.FieldByName(name)
	if !found {
		return false
	}
	val.FieldByIndex(field.Index).Set(reflect.ValueOf(fieldObj))
	return true
}

func CopyValueToModel(modelType reflect.Type, modelValue reflect.Value, newModel interface{}) {
	newValue := UnwrapValue(reflect.ValueOf(newModel))
	if !modelValue.IsValid() {
		newValue.Set(reflect.Zero(newValue.Type()))
		return
	}
	if modelValue.Kind() == reflect.Ptr && (modelValue.IsNil() || modelValue.IsZero()) {
		newValue.Set(modelValue)
		return
	}
	newType := UnwrapType(reflect.TypeOf(newModel))
	for i := 0; i < newType.NumField(); i++ {
		f := newValue.Field(i)
		modF := newType.Field(i)
		origF, found := modelType.FieldByName(modF.Name)
		if found {
			f.Set(modelValue.FieldByIndex(origF.Index))
		}
	}
}

func Copy(from interface{}, to interface{}) interface{} {
	CopyValueToModel(UnwrapType(reflect.TypeOf(to)), UnwrapValue(reflect.ValueOf(from)), EnsureSinglePointer(to))
	return to
}
