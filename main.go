// Example program to demonstrate the modules
package main

import (
	"fmt"
	"log"
	"net/http"

	"github.com/graphql-go/graphql"
	"gitlab.com/xdevs23/goqlorm/reflectutil"

	"gitlab.com/xdevs23/goqlorm/api"
	"gitlab.com/xdevs23/goqlorm/database/dbsystem"
	"gitlab.com/xdevs23/goqlorm/plugnplay"
)

func main() {
	var pluggedConfig *plugnplay.PluggedConfig
	pluggedConfig = plugnplay.Plug(&plugnplay.Config{
		// Only useful when using a DBMS that connects via network
		DefaultConnectionString: "localhost:5432",
		// Required!
		DBSystem: dbsystem.FromString("PostgreSQL"),
		// These three are only useful when using a DBMS that supports authentication
		DBUser:     "pguser",
		DBPassword: "pgpass",
		DBName:     "db",

		DBInsertOnMissingEntryToUpdate: true,
		Models: []interface{}{
			(*User)(nil),
			(*Thing)(nil),
			(*Secret)(nil),
		},
		ExcludeModelsFromGQL: []interface{}{
			(*Secret)(nil),
		},
		ApiHandlerConfig: api.HandlerConfig{
			OnRequest: func(w http.ResponseWriter, r *http.Request, config *api.HandlerConfig, ctx *api.GraphQLContextWrapper) error {
				// Database usage example
				// You can also use the pluggedConfig you get from the .Plug call,
				// this is just a convenient way of doing it.
				cfg := config.ExtraConfig.(*plugnplay.PluggedConfig)
				db := cfg.Database // you can use the ORM directly from here
				// We aren't actually doing anything with it, just showing off
				_ = db
				// You can do some authentication/verification in here
				return nil
			},
			PreProcess:      nil,
			PostProcess:     nil,
			PreWriteResult:  nil,
			PostWriteResult: nil,
			BeforeMutate: func(p graphql.ResolveParams, ctx *api.GraphQLContextWrapper, args interface{}, model interface{}) error {
				log.Println("Model", reflectutil.GetModelName(model), "is being mutated with args", args)
				return nil
			},
			OnQueryResult: func(p graphql.ResolveParams, ctx *api.GraphQLContextWrapper, result interface{}) error {
				log.Println("Got some query result:", result)
				return nil
			},
			DisableURLQuery:  false,
			DisableBodyQuery: false,
			Context:          nil,
			GraphQLContext:   nil,
		},
		CustomGQLQueries: graphql.Fields{
			"test": &graphql.Field{
				Name: "InterestingObject",
				Type: graphql.String,
				Args: graphql.FieldConfigArgument{
					"id": &graphql.ArgumentConfig{
						Type:        graphql.NewNonNull(graphql.Int),
						Description: "ID of your user",
					},
				},
				Resolve: func(p graphql.ResolveParams) (i interface{}, err error) {
					// This is how you can create your custom query and use the ORM
					user := &User{
						Id: int64(p.Args["id"].(int)),
					}
					if _, err = pluggedConfig.Database.Collection((*User)(nil)).Select(user); err != nil {
						return
					}
					return fmt.Sprintf("Hello, %s!", user.Name), nil
				},
				DeprecationReason: "",
				Description:       "Very interesting string",
			},
		},
	})
	if err := pluggedConfig.Play(); err != nil {
		panic(err)
	}
}
