// Package runtimeutil contains useful utilities
// that make use of the `runtime` package
package runtimeutil

import (
	"runtime"
)

func FuncNameOfCallerIndex(skip int) string {
	pc := make([]uintptr, skip+1)
	n := runtime.Callers(skip, pc)
	frames := runtime.CallersFrames(pc[:n])
	frame, _ := frames.Next()
	return frame.Function
}

func CallerFuncName() string {
	// 1: FuncNameOfCallerIndex
	// 2: CallerFuncName
	// 3: <Function calling this>
	// 4: <Caller function of function calling this>
	return FuncNameOfCallerIndex(4)
}
