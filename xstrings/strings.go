// Package xstrings contains custom string utilities
package xstrings

import "strings"

func ToCamelCase(s string) string {
	return strings.ToLower(s[:1]) + s[1:]
}
