package generator

import (
	"reflect"

	"gitlab.com/xdevs23/goqlorm/api"

	"gitlab.com/xdevs23/goqlorm/reflectutil"

	"github.com/graphql-go/graphql"
)

func (gen *GraphQLSchemaGenerator) AutoResolveQueryOne(model interface{}) graphql.FieldResolveFn {
	return gen.AutoResolve(model, false)
}

func (gen *GraphQLSchemaGenerator) AutoResolveQueryList(model interface{}) graphql.FieldResolveFn {
	return gen.AutoResolve(model, true)
}

func (gen *GraphQLSchemaGenerator) AutoResolve(model interface{}, returnList bool) graphql.FieldResolveFn {
	modelType := *prepareAutoResolve(model)
	return func(p graphql.ResolveParams) (result interface{}, err error) {
		objArr := reflectutil.NewFromType(reflect.SliceOf(modelType))
		err = gen.db.Collection(modelType).DataFromDatabaseUsingResolveParams(objArr, modelType, p)
		if err != nil {
			return
		}
		if returnList {
			result = objArr
		} else {
			elem := reflect.ValueOf(objArr).Elem()
			if elem.Len() == 0 {
				return nil, nil
			}
			result = elem.Index(0).Interface()
		}

		ctx, ok := p.Context.Value(api.MainContextKey).(*api.GraphQLContextWrapper)
		if !ok {
			// Use the shared one instead
			ctx = api.GraphiQLContextWrapper()
		}
		if ctx.HandlerConfig.OnQueryResult != nil {
			if err := ctx.HandlerConfig.OnQueryResult(p, ctx, result); err != nil {
				return nil, err
			}
		}

		return
	}
}
