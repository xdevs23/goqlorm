package generator

import (
	"github.com/graphql-go/graphql"
)

func (gen *GraphQLSchemaGenerator) GenerateQueryObject() *graphql.Object {
	return graphql.NewObject(
		graphql.ObjectConfig{
			Name:        "Query",
			Fields:      *gen.GenerateQueryFields(),
			Description: "Queries",
		})
}

func (gen *GraphQLSchemaGenerator) GenerateMutationObject() *graphql.Object {
	return graphql.NewObject(
		graphql.ObjectConfig{
			Name:        "Mutation",
			Fields:      *gen.GenerateMutationFields(),
			Description: "Mutations",
		})
}
