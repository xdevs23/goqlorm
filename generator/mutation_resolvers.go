package generator

import (
	"github.com/graphql-go/graphql"
	"github.com/joomcode/errorx"
	"gitlab.com/xdevs23/goqlorm/api"
)

func runBeforeMutate(p graphql.ResolveParams, obj interface{}, model interface{}) error {
	ctx, ok := p.Context.Value(api.MainContextKey).(*api.GraphQLContextWrapper)
	if !ok {
		// Use the shared one instead
		ctx = api.GraphiQLContextWrapper()
	}
	if ctx.HandlerConfig.BeforeMutate != nil {
		if err := ctx.HandlerConfig.BeforeMutate(p, ctx, obj, model); err != nil {
			return err
		}
	}
	return nil
}

func (gen *GraphQLSchemaGenerator) AutoResolveCreateOne(model interface{}) graphql.FieldResolveFn {
	modelType := *prepareAutoResolve(model)
	return func(p graphql.ResolveParams) (interface{}, error) {
		ptrObj, _ := ConstructObjectFromPArgs(modelType, &p)
		obj := *ptrObj

		if err := runBeforeMutate(p, obj, model); err != nil {
			return nil, err
		}

		if err := gen.db.Collection(obj).Insert(obj); err != nil {
			return nil, errorx.EnsureStackTrace(err)
		}
		return obj, nil
	}
}

func (gen *GraphQLSchemaGenerator) AutoResolveUpdateOne(model interface{}) graphql.FieldResolveFn {
	modelType := *prepareAutoResolve(model)
	return func(p graphql.ResolveParams) (interface{}, error) {
		ptrObj, specifiedFields := ConstructObjectFromPArgs(modelType, &p)
		obj := *ptrObj

		if err := runBeforeMutate(p, obj, model); err != nil {
			return nil, err
		}

		if err := gen.db.Collection(obj).UpdateSpecifiedFields(obj, specifiedFields); err != nil {
			return nil, err
		}
		return obj, nil
	}
}

func (gen *GraphQLSchemaGenerator) AutoResolveDeleteOne(model interface{}) graphql.FieldResolveFn {
	modelType := *prepareAutoResolve(model)
	return func(p graphql.ResolveParams) (interface{}, error) {
		ptrObj, _ := ConstructObjectFromPArgs(modelType, &p)
		obj := *ptrObj

		if err := runBeforeMutate(p, obj, model); err != nil {
			return nil, err
		}

		if err := gen.db.Collection(obj).Delete(obj); err != nil {
			return nil, err
		}
		return obj, nil
	}
}
