package generator

import (
	"reflect"

	"gitlab.com/xdevs23/goqlorm/xstrings"

	"gitlab.com/xdevs23/goqlorm/reflectutil"

	"github.com/graphql-go/graphql"

	"github.com/mitchellh/mapstructure"
)

// Generates a GraphQL object based on a model.
// Uses a cache to speed up subsequent calls and
// make sure there are no duplicates. The cache is
// very important because otherwise GraphQL will
// spit out weird errors as it expects every type
// to only exist once and gets easily confused.
func (gen *GraphQLSchemaGenerator) GetGraphQLObjType(model interface{}, shouldSkipRelations bool) *graphql.Object {
	return gen.convertStructToObject(reflectutil.UnwrapType(reflect.TypeOf(model)), shouldSkipRelations)
}

// Makes sure that the specified model fulfills all requirements
// and returns the reflection type of the specified model
func prepareAutoResolve(model interface{}) *reflect.Type {
	modelType := reflect.TypeOf(model).Elem()
	if modelType.Kind() != reflect.Struct {
		panic("model type is not struct, instead got " + modelType.Kind().String())
	}
	return &modelType
}

// Reads out the GraphQL arguments and writes them into
// a new object. Consider this as a kind of deserialization.
func ConstructObjectFromPArgs(modelType reflect.Type, p *graphql.ResolveParams) (*interface{}, []reflect.StructField) {
	obj := reflectutil.NewFromType(modelType)
	var specifiedFields []reflect.StructField
	for i := 0; i < modelType.NumField(); i++ {
		field := modelType.Field(i)
		fieldNameLowerCase := xstrings.ToCamelCase(field.Name)
		if value, ok := p.Args[fieldNameLowerCase]; ok {
			specifiedFields = append(specifiedFields, field)
			if value == nil {
				continue
			}
			s := reflect.ValueOf(obj).Elem()
			if modelType.Kind() == reflect.Struct {
				f := s.FieldByIndex(field.Index)
				if f.IsValid() {
					if f.CanSet() {
						if reflectutil.UnwrapValue(f).Kind() == reflect.Struct {
							if value.(map[string]interface{})["id"] == -1 {
								// ID -1 is invalid and thus assume it's supposed to mean null
								// as this GraphQL implementation does not understand explicit nulls
								continue
							}
							decodedValue := reflectutil.NewFromType(f.Type().Elem())
							if err := mapstructure.Decode(value, decodedValue); err != nil {
								// I guess
							}
							f.Set(reflect.ValueOf(decodedValue))
							continue
						}
						valType := reflect.TypeOf(value)
						kind := valType.Kind()
						switch kind {
						case reflect.Array:
							fallthrough
						case reflect.Slice:
							reflectutil.SetSlice(value.([]interface{}), f)
						case reflect.Int:
							f.SetInt(int64(value.(int)))
						case reflect.Float32:
							f.SetFloat(float64(value.(float32)))
						case reflect.Map:
							decodedValue := reflectutil.NewFromType(f.Type().Elem())
							if err := mapstructure.Decode(value, decodedValue); err != nil {
								// I guess
							}
							f.Set(reflect.ValueOf(decodedValue))
						default:
							f.Set(reflect.ValueOf(value))
						}
					}
				}
			}
		}
	}
	return &obj, specifiedFields
}
