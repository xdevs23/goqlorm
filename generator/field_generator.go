package generator

import (
	"fmt"
	"reflect"
	"strings"

	"gitlab.com/xdevs23/goqlorm/xstrings"

	"github.com/xdevs23/straf"

	"gitlab.com/xdevs23/goqlorm/reflectutil"

	"github.com/graphql-go/graphql"
)

const (
	generatorTagPrefix            = "goqlorm.generator."
	generatorTagRequired          = generatorTagPrefix + "required"
	generatorTagExclude           = generatorTagPrefix + "exclude"
	generatorTagDescription       = generatorTagPrefix + "description"
	generatorTagDeprecationReason = generatorTagPrefix + "deprecationReason"
	generatorTagIsArg             = generatorTagPrefix + "isArg"
)

var fieldsCache = map[string]map[bool]graphql.Fields{}

func (gen *GraphQLSchemaGenerator) convertStructInner(modelType reflect.Type, shouldSkipRelations bool) graphql.Fields {

	if cachedField := fieldsCache[modelType.Name()][shouldSkipRelations]; cachedField != nil {
		return cachedField
	}
	fields := graphql.Fields{}

	for i := 0; i < modelType.NumField(); i++ {
		currentField := modelType.Field(i)
		if shouldSkipRelations && gen.db.IsTypeRelation(modelType, currentField) {
			continue
		}

		if straf.GetTagValue(currentField, generatorTagExclude) != "true" {
			// skip relations for recursive sub input types
			fieldType := gen.getFieldType(modelType, currentField, false, true)
			camelCaseName := xstrings.ToCamelCase(currentField.Name)
			fieldConfig := &graphql.Field{
				Type:              graphql.NewNonNull(fieldType),
				Name:              camelCaseName,
				DeprecationReason: straf.GetTagValue(currentField, generatorTagDeprecationReason),
				Description:       straf.GetTagValue(currentField, generatorTagDescription),
			}
			if straf.GetTagValue(currentField, generatorTagRequired) == "true" {
				fieldConfig.Name += "!"
			} else {
				fieldConfig.Type = fieldType
			}
			fields[camelCaseName] = fieldConfig
		}
	}

	cacheEntry, ok := fieldsCache[modelType.Name()]
	if !ok {
		cacheEntry = map[bool]graphql.Fields{}
		fieldsCache[modelType.Name()] = cacheEntry
	}
	cacheEntry[shouldSkipRelations] = fields

	return fields
}

func (gen *GraphQLSchemaGenerator) convertStruct(modelType reflect.Type, shouldSkipRelations bool) graphql.FieldsThunk {
	return func() graphql.Fields {
		return gen.convertStructInner(modelType, shouldSkipRelations)
	}
}

var inputFieldsCache = map[string]*graphql.InputObjectConfigFieldMap{}

func (gen *GraphQLSchemaGenerator) convertStructToInputFieldsInner(modelType reflect.Type) graphql.InputObjectConfigFieldMap {
	if cachedField := inputFieldsCache[modelType.Name()]; cachedField != nil {
		return *cachedField
	}
	fields := graphql.InputObjectConfigFieldMap{}

	for i := 0; i < modelType.NumField(); i++ {
		currentField := modelType.Field(i)
		if straf.GetTagValue(currentField, generatorTagExclude) != "true" {
			fieldType := gen.getFieldType(modelType, currentField, true, false)
			camelCaseName := xstrings.ToCamelCase(currentField.Name)
			typ := fieldType
			if camelCaseName != "id" && straf.GetTagValue(currentField, generatorTagRequired) == "true" {
				typ = graphql.NewNonNull(typ)
			}
			fieldConfig := &graphql.InputObjectFieldConfig{
				Type:        typ,
				Description: straf.GetTagValue(currentField, generatorTagDescription),
			}
			if straf.GetTagValue(currentField, generatorTagRequired) == "false" {
				fieldConfig.Type = fieldType
			}
			fields[camelCaseName] = fieldConfig
		}
	}

	inputFieldsCache[modelType.Name()] = &fields

	return fields
}

func (gen *GraphQLSchemaGenerator) convertStructToInputFields(modelType reflect.Type) graphql.InputObjectConfigFieldMapThunk {
	return func() graphql.InputObjectConfigFieldMap {
		return gen.convertStructToInputFieldsInner(modelType)
	}
}

var objectTypeCache = map[string]map[bool]*graphql.Object{}

func (gen *GraphQLSchemaGenerator) convertStructToObject(modelType reflect.Type, shouldSkipRelations bool) *graphql.Object {
	if obj := objectTypeCache[modelType.Name()][shouldSkipRelations]; obj != nil {
		return obj
	}

	fields := gen.convertStruct(modelType, shouldSkipRelations)

	suffix := ""
	if shouldSkipRelations {
		suffix = "Simple"
	}

	obj := graphql.NewObject(
		graphql.ObjectConfig{
			Name:   modelType.Name() + suffix,
			Fields: fields,
		},
	)

	cacheEntry, ok := objectTypeCache[modelType.Name()]
	if !ok {
		cacheEntry = map[bool]*graphql.Object{}
		objectTypeCache[modelType.Name()] = cacheEntry
	}
	cacheEntry[shouldSkipRelations] = obj
	return obj
}

var inputObjectTypeCache = map[string]*graphql.InputObject{}

func (gen *GraphQLSchemaGenerator) convertStructToInputObject(modelType reflect.Type) *graphql.InputObject {
	if obj := inputObjectTypeCache[modelType.Name()]; obj != nil {
		return obj
	}

	fields := gen.convertStructToInputFields(modelType)

	obj := graphql.NewInputObject(
		graphql.InputObjectConfig{
			Name:        modelType.Name() + "Input",
			Description: fmt.Sprintf("Used as input type for %ss", modelType.Name()),
			Fields:      fields,
		},
	)
	inputObjectTypeCache[modelType.Name()] = obj
	return obj
}

type graphqlOutputWrapper struct {
	output graphql.Output
}

type fieldTypeCacheMap map[string]map[string]map[bool]*graphqlOutputWrapper

var fieldTypeCache = fieldTypeCacheMap{}
var inputFieldTypeCache = fieldTypeCacheMap{}

func (gen *GraphQLSchemaGenerator) getFieldType(
	modelType reflect.Type, object reflect.StructField, forInput bool, shouldSkipRelations bool) graphql.Type {

	if forInput {
		if cachedType := inputFieldTypeCache[modelType.Name()][object.Name][shouldSkipRelations]; cachedType != nil {
			return cachedType.output
		}
	} else {
		if cachedType := fieldTypeCache[modelType.Name()][object.Name][shouldSkipRelations]; cachedType != nil {
			return cachedType.output
		}
	}
	isID, ok := object.Tag.Lookup("unique")
	if isID == "true" && ok {
		return graphql.ID
	}

	wrapper := &graphqlOutputWrapper{}
	if forInput {
		inputFieldTypeCache.addToCache(modelType, object, wrapper, shouldSkipRelations)
	} else {
		fieldTypeCache.addToCache(modelType, object, wrapper, shouldSkipRelations)
	}

	objectType := reflectutil.UnwrapType(object.Type)
	if objectType.Kind() == reflect.Struct {
		if forInput {
			wrapper.output = gen.convertStructToInputObject(objectType)
		} else {
			wrapper.output = gen.convertStructToObject(objectType, shouldSkipRelations)
		}
	} else if objectType.Kind() == reflect.Slice &&
		objectType.Elem().Kind() == reflect.Struct {

		if forInput {
			wrapper.output = gen.convertStructToInputObject(objectType.Elem())
		} else {
			wrapper.output = gen.convertStructToObject(objectType.Elem(), shouldSkipRelations)
		}
		wrapper.output = graphql.NewList(wrapper.output)
	} else if objectType.Kind() == reflect.Slice {
		elemType, _ := straf.ConvertSimpleType(objectType.Elem())
		wrapper.output = graphql.NewList(elemType)
	} else {
		output, _ := straf.ConvertSimpleType(objectType)
		wrapper.output = output
	}

	return wrapper.output
}

func (ftc fieldTypeCacheMap) addToCache(
	modelType reflect.Type, object reflect.StructField, output *graphqlOutputWrapper, shouldSkipRelations bool) graphql.Type {
	ftcSub := ftc[modelType.Name()]
	if ftcSub == nil {
		ftcSub = map[string]map[bool]*graphqlOutputWrapper{}
		ftc[modelType.Name()] = ftcSub
	}
	if ftcSub[object.Name] == nil {
		ftcSub[object.Name] = map[bool]*graphqlOutputWrapper{}
	}
	ftcSub[object.Name][shouldSkipRelations] = output
	return output.output
}

const (
	// IDBehaviorSkip skips the Id field in input arguments
	IDBehaviorSkip IDBehavior = iota
	// IDBehaviorInclusive includes the Id field in input arguments among other arguments
	IDBehaviorInclusive
	// IDBehaviorExclusive exclusively includes the Id field in input arguments, skipping all others
	IDBehaviorExclusive
)

type IDBehavior int

func (gen *GraphQLSchemaGenerator) InputFieldConfigArgs(model interface{}, shouldSkipRelations bool, idBehavior IDBehavior) *graphql.FieldConfigArgument {

	fieldConfigArg := graphql.FieldConfigArgument{}
	modelType := reflectutil.UnwrapType(reflect.TypeOf(model))

	for i := 0; i < modelType.NumField(); i++ {
		currentField := modelType.Field(i)
		shouldBreak := false
		if currentField.Name == "Id" {
			switch idBehavior {
			case IDBehaviorSkip:
				continue
			case IDBehaviorExclusive:
				shouldBreak = true
			case IDBehaviorInclusive:
				// Include the Id field
			}
		}
		if shouldSkipRelations && gen.db.IsRelation(model, currentField) {
			continue
		}

		if straf.GetTagValue(currentField, generatorTagExclude) != "true" {
			isArg := currentField.Tag.Get(generatorTagIsArg)
			if isArg != "false" {
				fieldType := gen.getFieldType(modelType, currentField, true, shouldSkipRelations)
				camelCaseName := xstrings.ToCamelCase(currentField.Name)
				fieldConfigArg[camelCaseName] = &graphql.ArgumentConfig{
					Type:         fieldType,
					Description:  straf.GetTagValue(currentField, generatorTagDescription),
					DefaultValue: nil,
				}
			}
		}

		if shouldBreak {
			break
		}
	}

	return &fieldConfigArg
}

func (gen *GraphQLSchemaGenerator) QueryOne(model interface{}) *graphql.Field {
	objType := gen.GetGraphQLObjType(model, false)
	return &graphql.Field{
		Type:        objType,
		Description: fmt.Sprintf("Get one %s", xstrings.ToCamelCase(reflectutil.GetModelName(model))),
		Args:        *gen.InputFieldConfigArgs(model, true, IDBehaviorInclusive),
		Resolve:     gen.AutoResolveQueryOne(model),
	}
}

func (gen *GraphQLSchemaGenerator) QueryList(model interface{}) *graphql.Field {
	objType := gen.GetGraphQLObjType(model, false)
	return &graphql.Field{
		Type:        graphql.NewList(objType),
		Description: fmt.Sprintf("Get list of %ss", xstrings.ToCamelCase(reflectutil.GetModelName(model))),
		Args:        *gen.InputFieldConfigArgs(model, true, IDBehaviorInclusive),
		Resolve:     gen.AutoResolveQueryList(model),
	}
}

func (gen *GraphQLSchemaGenerator) GenerateQueryFields() *graphql.Fields {
	fields := graphql.Fields{}
	for _, model := range gen.models {
		name := xstrings.ToCamelCase(reflectutil.GetModelName(model))
		fields[name] = gen.QueryOne(model)
		fields[name+"s"] = gen.QueryList(model)
	}
	for key, value := range gen.customQueryFields {
		fields[key] = value
	}
	return &fields
}

func (gen *GraphQLSchemaGenerator) CreateOne(model interface{}) *graphql.Field {
	returnObjectType := gen.GetGraphQLObjType(model, false)
	return &graphql.Field{
		Type:        returnObjectType,
		Description: fmt.Sprintf("Create one %s", xstrings.ToCamelCase(reflectutil.GetModelName(model))),
		Args:        *gen.InputFieldConfigArgs(model, false, IDBehaviorSkip),
		Resolve:     gen.AutoResolveCreateOne(model),
	}
}

func (gen *GraphQLSchemaGenerator) UpdateOne(model interface{}) *graphql.Field {
	returnObjectType := gen.GetGraphQLObjType(model, false)
	return &graphql.Field{
		Type:        returnObjectType,
		Description: fmt.Sprintf("Update one %s", xstrings.ToCamelCase(reflectutil.GetModelName(model))),
		Args:        *gen.InputFieldConfigArgs(model, true, IDBehaviorInclusive),
		Resolve:     gen.AutoResolveUpdateOne(model),
	}
}

func (gen *GraphQLSchemaGenerator) DeleteOne(model interface{}) *graphql.Field {
	returnObjectType := gen.GetGraphQLObjType(model, true)
	return &graphql.Field{
		Type:        returnObjectType,
		Description: fmt.Sprintf("Delete one %s", xstrings.ToCamelCase(reflectutil.GetModelName(model))),
		Args:        *gen.InputFieldConfigArgs(model, true, IDBehaviorExclusive),
		Resolve:     gen.AutoResolveDeleteOne(model),
	}
}

func (gen *GraphQLSchemaGenerator) GenerateMutationFields() *graphql.Fields {
	fields := graphql.Fields{}
	for _, model := range gen.models {
		name := strings.Title(reflectutil.GetModelName(model))
		fields["create"+name] = gen.CreateOne(model)
		fields["update"+name] = gen.UpdateOne(model)
		fields["delete"+name] = gen.DeleteOne(model)
	}
	for key, value := range gen.customMutationFields {
		fields[key] = value
	}
	return &fields
}
