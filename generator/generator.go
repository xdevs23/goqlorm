package generator

import (
	"reflect"

	"gitlab.com/xdevs23/goqlorm/database"

	"github.com/graphql-go/graphql"
)

type GraphQLSchemaGenerator struct {
	db                   *database.Database
	models               []interface{}
	customQueryFields    graphql.Fields
	customMutationFields graphql.Fields
}

func GenerateGraphQLSchema(
	db *database.Database, excludeModels []interface{},
	customQueryFields graphql.Fields, customMutationFields graphql.Fields,
	models ...interface{}) (graphql.Schema, *GraphQLSchemaGenerator, error) {

	gen := GraphQLSchemaGenerator{
		db:                   db,
		customQueryFields:    customQueryFields,
		customMutationFields: customMutationFields,
	}
	if excludeModels != nil {
		for _, model := range models {
			for _, excludedModel := range excludeModels {
				if reflect.TypeOf(model) == reflect.TypeOf(excludedModel) {
					goto afterAppend
				}
			}
			gen.models = append(gen.models, model)
		afterAppend:
		}
	} else {
		gen.models = models
	}
	schema, err := graphql.NewSchema(
		graphql.SchemaConfig{
			Query:    gen.GenerateQueryObject(),
			Mutation: gen.GenerateMutationObject(),
		},
	)
	return schema, &gen, err
}

func (gen *GraphQLSchemaGenerator) Database() *database.Database {
	return gen.db
}
