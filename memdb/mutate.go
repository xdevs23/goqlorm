package memdb

import (
	"github.com/joomcode/errorx"
	"github.com/xdevs23/structs"
	"gitlab.com/xdevs23/goqlorm/reflectutil"
)

const idName = "Id"

func (db *Database) CreateCollectionFromModel(name string, _ interface{}, ifNotExists bool) error {
	defer db.mux.Unlock()
	db.mux.Lock()
	exists := db.dbContent.collections[name] != nil
	if !ifNotExists && exists {
		return errorx.IllegalState.New("collection %s already exists", name)
	} else if exists {
		return nil
	}
	db.dbContent.collections[name] = &collection{
		entries:   map[idCol]*collectionEntry{},
		sequences: map[columnName]*int64{},
	}
	return nil
}

func (db *Database) prepareMutation(opType string, cn string, model interface{}) (
	*collection, *collectionEntry, int64, error) {

	collection := db.dbContent.collections[cn]
	if collection == nil {
		return collection, nil, -1, errorx.IllegalState.New("can't %s, collection %s does not exist", opType, cn)
	}
	id := reflectutil.GetFieldByName(model, idName)
	if id == nil {
		return collection, nil, -1, errorx.IllegalState.New("can't %s, %s field missing", opType, idName)
	}
	switch id.(type) {
	case int64:
		// you shall drive thru
	default:
		return collection, nil, -1, errorx.IllegalState.New("can't %s, expected %s field type int64", opType, idName)
	}
	return collection, collection.entries[id.(int64)], id.(int64), nil
}

func (db *Database) Insert(cn string, model interface{}) error {
	defer db.mux.Unlock()
	db.mux.Lock()
	collection, id, err := db.preInsert(cn, model)
	if err != nil {
		return err
	}
	db.persistInsert(collection, id, model)
	return nil
}

func (db *Database) persistInsert(collection *collection, id int64, model interface{}) {
	collection.entries[id] = &collectionEntry{structs.Map(model)}
}

func (db *Database) preInsert(cn string, model interface{}) (*collection, int64, error) {
	collection, entry, id, err := db.prepareMutation("insert", cn, model)
	if err != nil {
		return nil, -1, err
	}
	if entry != nil {
		return nil, -1, errorx.IllegalState.New(
			"can't insert, entry with %s %v already exists", idName, id)
	}
	if id < 1 {
		id = collection.seq(idName)
	}
	if !reflectutil.SetFieldByName(model, idName, id) {
		return nil, -1, errorx.IllegalState.New("couldn't set %s in model", idName)
	}
	return collection, id, nil
}

func (db *Database) Update(cn string, model interface{}) error {
	defer db.mux.Unlock()
	db.mux.Lock()
	collection, id, err := db.preUpdate(cn, model)
	if err != nil {
		return err
	}
	db.persistUpdate(collection, id, model)
	return nil
}

func (db *Database) persistUpdate(collection *collection, id int64, model interface{}) {
	collection.entries[id].columns = structs.Map(model)
}

func (db *Database) preUpdate(cn string, model interface{}) (*collection, int64, error) {
	defer db.mux.Unlock()
	db.mux.Lock()
	collection, entry, id, err := db.prepareMutation("update", cn, model)
	if err != nil {
		return nil, -1, err
	}
	if entry == nil {
		return nil, -1, errorx.IllegalState.New(
			"can't update, entry with %s %v does not exist", idName, id)
	}
	return collection, id, nil
}

func (db *Database) Delete(cn string, model interface{}) error {
	defer db.mux.Unlock()
	db.mux.Lock()
	collection, id, err := db.preDelete(cn, model)
	if err != nil {
		return err
	}
	db.persistDelete(collection, id)
	return nil
}

func (db *Database) persistDelete(collection *collection, id int64) {
	delete(collection.entries, id)
}

func (db *Database) preDelete(cn string, model interface{}) (*collection, int64, error) {
	collection, entry, id, err := db.prepareMutation("delete", cn, model)
	if err != nil {
		return nil, -1, err
	}
	if entry == nil {
		return nil, -1, errorx.IllegalState.New(
			"can't delete, entry with %s %v does not exist", idName, id)
	}
	return collection, id, nil
}
