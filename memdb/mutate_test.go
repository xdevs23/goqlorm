package memdb

import (
	"log"
	"sync"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"

	"gitlab.com/xdevs23/goqlorm/database"
	"gitlab.com/xdevs23/goqlorm/database/dbdefs"
	"gitlab.com/xdevs23/goqlorm/database/dbsystem"
)

const memDbName = "memdb"

type Embedded struct {
	str   string
	float float64
}

type Sub struct {
	Id                        int64
	TestStr                   string
	Arr                       []string
	Embedded                  Embedded
	TestWithThisAsSub         *Test  `goqlorm.relation.type:"one to one" goqlorm.relation.inversedBy:"Sub"`
	TestsWithThisAsSharedSub  []Test `goqlorm.relation.type:"one to many" goqlorm.relation.inversedBy:"SharedSub"`
	TestWithThisInSubs        *Test  `goqlorm.relation.type:"many to one" goqlorm.relation.inversedBy:"Subs"`
	TestsWithThisInSharedSubs []Test `goqlorm.relation.type:"many to many" goqlorm.relation.inversedBy:"SharedSubs"`
}

type Test struct {
	Id          int64
	Str         string
	Float       float64
	Sub         *Sub  `goqlorm.relation.type:"one to one"`
	SharedSub   *Sub  `goqlorm.relation.type:"many to one"`
	Subs        []Sub `goqlorm.relation.type:"one to many"`
	SharedSubs  []Sub `goqlorm.relation.type:"many to many"`
	EmbeddedArr []Embedded
}

func createTestObj() *Test {
	return &Test{
		Str:   "good str",
		Float: 41.2,
		Sub: &Sub{
			TestStr: "zz",
			Arr:     []string{"a", "b", "c"},
			Embedded: Embedded{
				str:   "str 0174u",
				float: 94.2,
			},
			TestsWithThisAsSharedSub:  []Test{},
			TestsWithThisInSharedSubs: []Test{},
		},
		SharedSub: &Sub{
			TestStr: "yy",
			Arr:     []string{"d", "e", "f"},
			Embedded: Embedded{
				str:   "str 1941a",
				float: 4951.0,
			},
			TestsWithThisAsSharedSub:  []Test{},
			TestsWithThisInSharedSubs: []Test{},
		},
		Subs: []Sub{
			{
				TestStr: "eee",
				Arr: []string{
					"fa",
					"fb",
					"fc",
					"fde",
				},
				Embedded: Embedded{
					str:   "3u1ic",
					float: 2414.42,
				},
				TestsWithThisAsSharedSub:  []Test{},
				TestsWithThisInSharedSubs: []Test{},
			},
			{
				TestStr: "ttt",
				Arr: []string{
					"ca",
					"cb",
					"cc",
					"cde",
				},
				Embedded: Embedded{
					str:   "3u6ic",
					float: 241.42,
				},
				TestsWithThisAsSharedSub:  []Test{},
				TestsWithThisInSharedSubs: []Test{},
			},
		},
		SharedSubs: []Sub{
			{
				TestStr: "nnn",
				Arr: []string{
					"ua",
					"ub",
					"uc",
					"ude",
				},
				Embedded: Embedded{
					str:   "95m1vks",
					float: 69.12,
				},
				TestsWithThisAsSharedSub:  []Test{},
				TestsWithThisInSharedSubs: []Test{},
			},
		},
		EmbeddedArr: []Embedded{
			{
				str:   "fsv e355",
				float: 0.0,
			},
			{
				str:   "fl0vx 8q",
				float: 12.0,
			},
		},
	}
}

func prepare() (*database.Database, error) {
	dbsystem.Register("memdb", func(config *dbsystem.DBAdapterConfig) dbdefs.Adapter {
		return NewDB(config)
	})
	db := database.Plug(dbsystem.FromString(memDbName), &dbsystem.DBAdapterConfig{}, dbdefs.SchemaSyncOptions{},
		(*Test)(nil), (*Sub)(nil))
	err := db.Prepare()
	return db, err
}

func TestDatabase_Insert(t *testing.T) {
	db, err := prepare()
	assert.NoError(t, err)
	assert.NoError(t, db.Prepare())
	testObj := createTestObj()
	selectTestObj := &Test{}
	assert.NoError(t, db.Collection(testObj).Insert(testObj))
	assert.NoError(t, db.Collection(testObj).SelectInto(testObj, selectTestObj))
	assert.Equal(t, testObj, selectTestObj)
}

func testDatabaseUpdate(t *testing.T, db *database.Database) {
	testObj := createTestObj()
	selectTestObj := &Test{}
	assert.NoError(t, db.Collection(testObj).Insert(testObj))
	assert.NoError(t, db.Collection(testObj).SelectInto(testObj, selectTestObj))
	assert.Equal(t, testObj, selectTestObj)
	updateTestObj := selectTestObj
	updateTestObj.Float = 64.1
	updateTestObj.Str = "432432"
	updateTestObj.Sub.Embedded.str = "a aac cc cc"
	updateTestObj.SharedSubs[0].TestStr = "3"
	updateTestObj.Subs[0].Arr = append(updateTestObj.Subs[0].Arr, "nice")
	testObj.Float = 64.1
	testObj.Str = "432432"
	testObj.Sub.Embedded.str = "a aac cc cc"
	testObj.SharedSubs[0].TestStr = "3"
	testObj.Subs[0].Arr = append(testObj.Subs[0].Arr, "nice")
	assert.NoError(t, db.Collection(updateTestObj).Update(updateTestObj))
	selectTestObj = createTestObj()
	assert.NoError(t, db.Collection(updateTestObj).SelectInto(updateTestObj, selectTestObj))
	assert.Equal(t, testObj, selectTestObj)
	assert.Equal(t, updateTestObj, selectTestObj)
	assert.Equal(t, testObj, updateTestObj)
	log.Println("ye")
}

func TestDatabase_Update(t *testing.T) {
	db, err := prepare()
	assert.NoError(t, err)
	assert.NoError(t, db.Prepare())
	testDatabaseUpdate(t, db)

	// Test thread safety
	var wg sync.WaitGroup
	wg.Add(1000)
	for i := 0; i < 1000; i++ {
		go func() {
			defer wg.Done()
			time.Sleep(time.Duration(i) * time.Microsecond)
			testDatabaseUpdate(t, db)
		}()
	}
	wg.Wait()
}

func TestDatabase_UpdateOne(t *testing.T) {
	db, err := prepare()
	assert.NoError(t, err)
	assert.NoError(t, db.Prepare())
	testDatabaseUpdate(t, db)
}

func TestDatabase_Delete(t *testing.T) {
	db, err := prepare()
	assert.NoError(t, err)
	assert.NoError(t, db.Prepare())
	testObj := createTestObj()
	selectTestObj := &Test{}
	assert.NoError(t, db.Collection(testObj).Insert(testObj))
	assert.NoError(t, db.Collection(testObj).SelectInto(testObj, selectTestObj))
	assert.Equal(t, testObj, selectTestObj)
	assert.NoError(t, db.Collection(testObj).Delete(testObj))
	result, err := db.Collection(testObj).Select(testObj)
	assert.NoError(t, err)
	assert.Nil(t, result)
}

func BenchmarkDatabase_Delete(b *testing.B) {
	db, _ := prepare()
	testObj := createTestObj()
	b.ResetTimer()
	for n := 0; n < b.N; n++ {
		b.StopTimer()
		_ = db.Collection(testObj).Insert(testObj)
		b.StartTimer()
		_ = db.Collection(testObj).Delete(testObj)
	}
}

func BenchmarkDatabase_Insert(b *testing.B) {
	db, _ := prepare()
	testObj := createTestObj()
	b.ResetTimer()
	for n := 0; n < b.N; n++ {
		_ = db.Collection(testObj).Insert(testObj)
	}
}

func BenchmarkDatabase_Update(b *testing.B) {
	db, _ := prepare()
	testObj := createTestObj()
	_ = db.Collection(testObj).Insert(testObj)
	b.ResetTimer()
	for n := 0; n < b.N; n++ {
		_ = db.Collection(testObj).Update(testObj)
	}
}
