package memdb

import (
	"gitlab.com/xdevs23/goqlorm/database/dbdefs"
	"gitlab.com/xdevs23/goqlorm/reflectutil"
)

func (db Database) SyncSchema(options dbdefs.SchemaSyncOptions, models ...interface{}) error {
	for i, model := range models {
		modelName := ""
		if i < len(options.ModelNames) {
			modelName = options.ModelNames[i]
			if len(modelName) == 0 {
				modelName = reflectutil.GetModelName(model)
			}
		} else {
			modelName = reflectutil.GetModelName(model)
		}
		if err := db.CreateCollectionFromModel(modelName, model, true); err != nil {
			return err
		}
	}
	return nil
}
