package memdb

func (db Database) Close() error {
	return nil
}

func (db Database) IsConnected() bool {
	return true
}
