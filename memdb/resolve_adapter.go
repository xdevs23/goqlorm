package memdb

import (
	"errors"
	"reflect"

	"gitlab.com/xdevs23/goqlorm/reflectutil"

	"gitlab.com/xdevs23/goqlorm/database/dbdefs"

	"gitlab.com/xdevs23/goqlorm/xstrings"

	"github.com/joomcode/errorx"

	"github.com/graphql-go/graphql"
)

func (db Database) DataFromDatabaseUsingResolveParams(
	objArr interface{},
	modelType reflect.Type,
	p graphql.ResolveParams) error {

	q := dbdefs.SelectQuery{
		Conditions:     map[dbdefs.ColumnName]dbdefs.ConditionValue{},
		IgnoreMultiple: false,
	}
	for i := 0; i < modelType.NumField(); i++ {
		field := modelType.Field(i)
		fieldNameLowerCase := xstrings.ToCamelCase(field.Name)
		if value := p.Args[fieldNameLowerCase]; value != nil {
			switch field.Type.Kind() {
			case
				reflect.Array,
				reflect.Slice:
				// TODO: implement this
			case
				reflect.Complex128,
				reflect.Complex64,
				reflect.Float64,
				reflect.Float32,
				reflect.Bool,
				reflect.Int8,
				reflect.Uint,
				reflect.Uint8,
				reflect.Uint16,
				reflect.Uint64,
				reflect.Uint32,
				reflect.Int16,
				reflect.Int64,
				reflect.Int32,
				reflect.Int,
				reflect.String:
				q.Conditions[field.Name] = value
			case reflect.Map:
				return errorx.EnsureStackTrace(errors.New("maps are not supported yet"))
			}
		}
	}

	return db.SelectAdvanced(reflectutil.GetModelName(objArr), objArr, &q)
}
