package memdb

import (
	"log"
	"sync"

	"gitlab.com/xdevs23/goqlorm/database/dbdefs"
)

type txStepFulfilmentFunc func() error

type transaction struct {
	db    *Database
	steps []txStepFulfilmentFunc
	mux   sync.Mutex
}

func (db *Database) RunInTransaction(fn dbdefs.InnerTransactionFunc) error {
	tx := &transaction{db: db}
	defer db.txMux.Unlock()
	log.Println("Tryna lock")
	db.txMux.Lock()
	log.Println("YOU GOTTA WAIT")
	if err := fn(tx); err != nil {
		tx.Revert()
		return err
	}
	err := tx.Commit()
	log.Println("DONE")
	return err
}

func (tx *transaction) CreateCollectionFromModel(name string, model interface{}, ifNotExists bool) error {
	defer tx.mux.Unlock()
	tx.mux.Lock()
	tx.steps = append(tx.steps, func() error {
		return tx.db.CreateCollectionFromModel(name, model, ifNotExists)
	})

	return nil
}

func (tx *transaction) Insert(collectionName string, obj interface{}) error {
	defer tx.mux.Unlock()
	tx.mux.Lock()
	collection, id, err := tx.db.preInsert(collectionName, obj)
	tx.steps = append(tx.steps, func() error {
		defer tx.db.mux.Unlock()
		tx.db.mux.Lock()
		if err != nil {
			return err
		}
		tx.db.persistInsert(collection, id, obj)
		return nil
	})
	return err
}

func (tx *transaction) Update(collectionName string, obj interface{}) error {
	defer tx.mux.Unlock()
	tx.mux.Lock()
	_, _, err := tx.db.preUpdate(collectionName, obj)
	tx.steps = append(tx.steps, func() error {
		if err != nil {
			return err
		}
		collection, id, err := tx.db.preUpdate(collectionName, obj)
		tx.db.persistUpdate(collection, id, obj)
		return err
	})
	return err
}

func (tx *transaction) Delete(collectionName string, obj interface{}) error {
	defer tx.mux.Unlock()
	tx.mux.Lock()
	collection, id, err := tx.db.preDelete(collectionName, obj)
	tx.steps = append(tx.steps, func() error {
		defer tx.db.mux.Unlock()
		tx.db.mux.Lock()
		if err != nil {
			return err
		}
		tx.db.persistDelete(collection, id)
		return nil
	})
	return err
}

func (tx *transaction) Commit() error {
	defer tx.mux.Unlock()
	tx.mux.Lock()
	for _, fn := range tx.steps {
		if err := fn(); err != nil {
			tx.Revert()
			return err
		}
	}
	return nil
}

func (tx *transaction) Revert() {
	defer tx.mux.Unlock()
	tx.mux.Lock()
	tx.steps = []txStepFulfilmentFunc{}
}
