// Package register is responsible for
// registering this database system
package register

import (
	"gitlab.com/xdevs23/goqlorm/database/dbdefs"
	"gitlab.com/xdevs23/goqlorm/database/dbsystem"
	"gitlab.com/xdevs23/goqlorm/memdb"
)

// Register the database system
func init() {
	dbsystem.Register("memdb", func(config *dbsystem.DBAdapterConfig) dbdefs.Adapter {
		return memdb.NewDB(config)
	})
}
