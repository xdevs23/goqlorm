package memdb

import (
	"reflect"
	"sort"

	"github.com/joomcode/errorx"
	"github.com/mitchellh/mapstructure"
	"gitlab.com/xdevs23/goqlorm/database/dbdefs"
	"gitlab.com/xdevs23/goqlorm/reflectutil"
)

func (db *Database) Select(collectionName string, args interface{}) (interface{}, error) {
	entry, i, err, done := db.prepareSelect(args, collectionName)
	if done {
		return i, err
	}

	newObj := reflectutil.NewFromType(reflectutil.PurifyToType(args))
	err = mapstructure.Decode(entry.columns, newObj)
	reflect.ValueOf(args).Elem().Set(reflect.ValueOf(newObj).Elem())

	return newObj, err
}

func (db *Database) prepareSelect(args interface{}, collectionName string) (*collectionEntry, interface{}, error, bool) {
	idUnCast := reflectutil.GetFieldByName(args, idName)
	if idUnCast == nil {
		return nil, nil, errorx.IllegalArgument.New("can't select, missing field %s in args", idName), true
	}
	switch idUnCast.(type) {
	case int64:
		// you shall pass thru
	default:
		return nil, nil, errorx.IllegalFormat.New("can't select, expected %s field of type int64", idName), true
	}
	id := idUnCast.(int64)

	collection, found := db.dbContent.collections[collectionName]
	if !found {
		return nil, nil, errorx.IllegalState.New("can't select, collection %s does not exist", collectionName), true
	}

	entry, found := collection.entries[id]
	if !found {
		return nil, nil, nil, true
	}
	return entry, nil, nil, false
}

func (db *Database) SelectInto(collectionName string, args interface{}, model interface{}) error {
	entry, _, err, done := db.prepareSelect(args, collectionName)
	if done {
		return err
	}

	return mapstructure.Decode(entry.columns, model)
}

func (db *Database) SelectAdvanced(collectionName string, model interface{}, query *dbdefs.SelectQuery) error {
	defer db.mux.RUnlock()
	db.mux.RLock()

	collection, found := db.dbContent.collections[collectionName]
	if !found {
		return errorx.IllegalState.New("can't select, collection %s does not exist", collectionName)
	}

	pureType := reflectutil.PurifyToType(model)
	var results []interface{}
	var idList []idCol
	for id, _ := range collection.entries {
		idList = append(idList, id)
	}
	sort.SliceStable(idList, func(i, j int) bool {
		return idList[i] < idList[j]
	})

	if len(query.Conditions) > 0 {
		for _, id := range idList {
			entry := collection.entries[id]
			for where, equals := range query.Conditions {
				whereObj, found := entry.columns[where]
				if !found {
					continue
				}
				if whereObj == equals {
					newObj := reflectutil.NewFromType(pureType)
					if err := mapstructure.Decode(entry.columns, newObj); err != nil {
						return err
					}
					results = append(results, newObj)
					if query.IgnoreMultiple {
						goto breakEntryLoop
					}
				}
			}
			continue
		breakEntryLoop:
			break
		}
	} else {
		for _, id := range idList {
			entry := collection.entries[id]
			newObj := reflectutil.NewFromType(pureType)
			if err := mapstructure.Decode(entry.columns, newObj); err != nil {
				return err
			}
			results = append(results, newObj)
		}
	}

	if query.IgnoreMultiple {
		if len(results) > 0 {
			reflect.ValueOf(model).Elem().Set(reflect.ValueOf(results[0]).Elem())
		}
	} else {
		newSlice := reflectutil.NewSliceOfTypeWithSize(pureType, len(results), len(results))
		newSliceVal := reflect.ValueOf(newSlice)
		for i, result := range results {
			newVal := newSliceVal.Index(i).Interface()
			newValPtr := reflectutil.EnsureSinglePointer(newVal)
			reflectutil.Copy(result, newValPtr)
			newSliceVal.Index(i).Set(reflect.ValueOf(newValPtr).Elem())
		}
		reflect.ValueOf(model).Elem().Set(newSliceVal)
	}

	return nil
}
