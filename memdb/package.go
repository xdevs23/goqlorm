// Package memdb provides an adapter for the database
// abstraction layer so that you can access in-memory databases
package memdb

import (
	"sync"

	"gitlab.com/xdevs23/goqlorm/database/dbsystem"
)

type Database struct {
	dbContent *databaseContent
	mux       *sync.RWMutex
	txMux     *sync.Mutex
}

type idCol = int64

type collection struct {
	entries   map[idCol]*collectionEntry
	sequences map[columnName]*int64
	mux       sync.Mutex
}

type columnName = string

type collectionEntry struct {
	columns map[columnName]interface{}
}

type collectionName = string

type databaseContent struct {
	collections map[collectionName]*collection
}

type databaseName = string

var databases = map[databaseName]*Database{}

func (c *collection) seq(column string) int64 {
	defer c.mux.Unlock()
	c.mux.Lock()
	seq := c.sequences[column]
	if seq == nil {
		lastSeqInt := int64(0)
		seq = &lastSeqInt
		c.sequences[column] = seq
	}
	found := true
	for found {
		*seq++
		_, found = c.entries[*seq]
	}
	return *seq
}

func NewDB(config *dbsystem.DBAdapterConfig) *Database {
	if existingDB, found := databases[config.DBName]; found {
		return existingDB
	}
	db := &Database{
		dbContent: &databaseContent{map[collectionName]*collection{}},
		mux:       &sync.RWMutex{},
		txMux:     &sync.Mutex{},
	}
	databases[config.DBName] = db
	return db
}
