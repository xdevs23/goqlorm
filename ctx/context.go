// Package ctx contains a default context if no custom one is used
package ctx

// Context is passed to every GraphQL resolver
// You can use your own. This is just the default.
type Context struct {
}
