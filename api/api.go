// Generates a GraphQL API handler for your convenience
package api

import (
	"context"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"

	"gitlab.com/xdevs23/goqlorm/ctx"

	"github.com/graphql-go/graphql"
)

// MainContextKey is used to provide a structured context object
// to all GraphQL resolvers
const MainContextKey = "mainContext"

// HTTPHandlerFuncWithError is the type used for middlewares
type HTTPHandlerFuncWithError func(w http.ResponseWriter, r *http.Request, config *HandlerConfig, wrapper *GraphQLContextWrapper) error

type GraphQLQueryResolverInterceptorFunc func(
	p graphql.ResolveParams, ctx *GraphQLContextWrapper, result interface{}) error

type GraphQLMutationResolverInterceptorFunc func(
	p graphql.ResolveParams, ctx *GraphQLContextWrapper, args interface{}, model interface{}) error

// HandlerConfig allows you to attach middlewares
// and configure the behavior as you like
type HandlerConfig struct {
	// Runs before any processing is done for requests
	OnRequest HTTPHandlerFuncWithError
	// Runs before processing the GraphQL query but after
	// doing some initial processing
	PreProcess HTTPHandlerFuncWithError
	// Runs right after the GraphQL query has been processed
	// and before the result is checked for errors
	PostProcess HTTPHandlerFuncWithError
	// Runs after checking for errors in the GraphQL query result
	// and before writing the result to the client. Only runs if
	// no errors were encountered.
	PreWriteResult HTTPHandlerFuncWithError
	// Runs after writing the result to the client. This is the last
	// of all handlers that is run.
	PostWriteResult HTTPHandlerFuncWithError
	// Runs before returning the response object from the GraphQL
	// query resolver so that you can intercept those objects
	OnQueryResult GraphQLQueryResolverInterceptorFunc
	// Runs before beginning a database transaction for mutations
	// in the GraphQL mutation resolver so that you can intercept
	// mutations before they are persisted
	BeforeMutate GraphQLMutationResolverInterceptorFunc

	// Specifying `true` will disallow using the URL query parameter
	// as a means of transporting the GraphQL query
	DisableURLQuery bool
	// Specifying `true` will disallow using the POST body
	// as a means of transporting the GraphQL query
	DisableBodyQuery bool

	// A context of your choice - this can contain anything.
	// This is different from the GraphQL context.
	Context interface{}

	// This context is passed down to all GraphQL resolvers inside
	// a predefined struct
	GraphQLContext interface{}

	// In case Plug-n-Play was used, this is a PluggedConfig.
	// Otherwise you can supply anything you want here.
	ExtraConfig interface{}
}

type GraphQLContextWrapper struct {
	HandlerConfig  *HandlerConfig
	Channel        chan interface{}
	AdditionalData []interface{}
	IsGraphiQL     bool
}

type requestError struct {
	Message string `json:"message"`
}

type requestErrors struct {
	// Errors contains human-readable error messages
	Errors []requestError `json:"errors"`
	// ErrorList resembles the originally passed error strings
	ErrorList []string `json:"errorList"`
}

// runMiddleware runs the handler and uses the encoder to
// write errors as responses. Parameters w and r are both
// passed to the handler. In case of an error, HTTP status
// code 500 will be sent to the client along with an error
// message inside a JSON
func runMiddleware(
	handler HTTPHandlerFuncWithError, config *HandlerConfig, wrapper *GraphQLContextWrapper,
	encoder *json.Encoder, w http.ResponseWriter, r *http.Request) {

	if handler != nil {
		if err := handler(w, r, config, wrapper); err != nil {
			w.WriteHeader(500)
			if encoder != nil {
				_ = encoder.Encode(requestErrors{
					[]requestError{
						{err.Error()}},
					[]string{err.Error()},
				})
			}
		}
	}
}

var graphiqlContextWrapper *GraphQLContextWrapper

// GenerateGraphQLAPIHandler generates a HTTP handler func that
// prepares and processes requests to the GraphQL API in an
// automated fashion
func GenerateGraphQLAPIHandler(schema *graphql.Schema, config *HandlerConfig) http.HandlerFunc {
	// Check if the config is valid
	if config.DisableBodyQuery && config.DisableURLQuery {
		// Only one can be disabled, otherwise requests wouldn't work
		panic("GenerateGraphQLAPIHandler: can't disable both body and URL queries")
	}

	// In case no context was passed, create a default one
	// just so that we have something to pass around
	if config.GraphQLContext == nil {
		config.GraphQLContext = &ctx.Context{}
	}

	// This is a shared one used when GraphiQL wants a context
	graphiqlContextWrapper = &GraphQLContextWrapper{
		HandlerConfig: config,
		IsGraphiQL:    true,
	}

	// This is the actual function that runs when a request comes in
	return func(w http.ResponseWriter, r *http.Request) {
		separateContextWrapper := &GraphQLContextWrapper{
			HandlerConfig: config,
		}

		responseEncoderWriter := json.NewEncoder(w)
		runMiddleware(config.OnRequest, config, separateContextWrapper, responseEncoderWriter, w, r)

		requestString := ""

		if r.Method == "POST" {
			if config.DisableBodyQuery {
				w.WriteHeader(400)
				_ = responseEncoderWriter.Encode(requestErrors{[]requestError{{
					"body queries are disabled, please use URL queries (.../?query=...)",
				}}, []string{"body_queries_disabled"}})
				return
			}
			bytes, err := io.ReadAll(r.Body)
			if err != nil {
				log.Println(err.Error())
				return
			}
			requestString = string(bytes)
		} else {
			if config.DisableURLQuery {
				w.WriteHeader(400)
				_ = responseEncoderWriter.Encode(requestErrors{[]requestError{{
					"URL queries are disabled, please use body queries (query in a POST body)",
				}}, []string{"url_query_disabled"}})
				return
			}
			requestString = r.URL.Query().Get("query")
		}

		runMiddleware(config.PreProcess, config, separateContextWrapper, responseEncoderWriter, w, r)

		result := graphql.Do(graphql.Params{
			Schema:        *schema,
			RequestString: requestString,
			Context:       context.WithValue(context.Background(), MainContextKey, separateContextWrapper),
		})

		runMiddleware(config.PostProcess, config, separateContextWrapper, responseEncoderWriter, w, r)

		if len(result.Errors) > 0 {
			errString := fmt.Sprintf("wrong result, unexpected errors: %v", result.Errors)
			var errorList []string
			for _, formattedErr := range result.Errors {
				errorList = append(errorList, formattedErr.Message)
			}
			log.Printf("%s\n", errString)
			if err := responseEncoderWriter.Encode(
				requestErrors{[]requestError{{errString}}, errorList}); err != nil {
				log.Println(err.Error())
			}
			return
		}

		runMiddleware(config.PreWriteResult, config, separateContextWrapper, responseEncoderWriter, w, r)

		if err := responseEncoderWriter.Encode(result); err != nil {
			log.Println(err.Error())
		}

		runMiddleware(config.PostWriteResult, config, separateContextWrapper, responseEncoderWriter, w, r)
	}

}

func GraphiQLContextWrapper() *GraphQLContextWrapper {
	return graphiqlContextWrapper
}
