package main

// User is a user
type User struct {
	Id           int64
	Name         string   `goqlorm.generator.description:"Name of the user"`
	Emails       []string `goqlorm.generator.description:"E-Mails of the user" goqlorm.generator.required:"false"`
	Thing        *Thing   `pg:"-" goqlorm.relation.type:"one to one"`
	AnotherThing *Thing   `pg:"-" goqlorm.relation.type:"many to one"`
	AllTheThings []Thing  `pg:"-" goqlorm.relation.type:"many to many"`
}
