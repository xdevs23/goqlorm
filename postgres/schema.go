package postgres

import (
	"errors"
	"fmt"
	"log"
	"reflect"
	"regexp"
	"strings"

	"gitlab.com/xdevs23/goqlorm/database/dbdefs"

	"gitlab.com/xdevs23/goqlorm/reflectutil"

	"github.com/joomcode/errorx"

	"github.com/go-pg/pg/v9/orm"
)

//noinspection GoStructTag pg wants tableName
type table struct {
	tableName    struct{} `pg:"information_schema.tables"`
	TableNameStr string   `pg:"table_name"`
}

//noinspection GoStructTag pg wants tableName
type column struct {
	tableName    struct{} `pg:"information_schema.columns"`
	ColumnName   string
	IsNullable   bool
	DataType     string
	TableNameStr string `pg:"table_name"`
}

type columnSlice []column

func (db Database) SyncSchema(options dbdefs.SchemaSyncOptions, models ...interface{}) error {
	for i, model := range models {
		modelType := reflectutil.UnwrapType(reflect.TypeOf(model))
		generatedTable := orm.GetTable(reflect.TypeOf(model).Elem())
		if options.ModelNames != nil && i < len(options.ModelNames) && len(options.ModelNames[i]) > 0 {
			generatedTable.Name = options.ModelNames[i]
		} else if len(modelType.Name()) == 0 {
			return errorx.IllegalArgument.New(
				"Model %d without a name was specified and the model name couldn't be found in options. "+
					"Please specify model names in the options for the models that don't have a name", i)
		} else {
			generatedTable.Name = modelType.Name()
		}
		pgTableName := ToPgTableName(generatedTable.Name)
		table := table{}
		if err := db.PgDB.Model(&table).Where("table_name = ?", pgTableName).Select(); err != nil {
			log.Println("Creating table", pgTableName)
			errCreate := db.tableModelQuery(generatedTable.Name, model, false).
				CreateTable(&orm.CreateTableOptions{})

			if errCreate != nil {
				return errors.New(err.Error() + "\n" + errCreate.Error())
			}
			// Table is created, no need to check columns
			continue
		}
		columns := make(columnSlice, 4)
		if err := db.PgDB.Model(&columns).Where("table_name = ?", table.TableNameStr).Select(); err != nil {
			return err
		}
		for _, field := range generatedTable.Fields {
			dbColumn := columns.find(func(col column) bool { return col.ColumnName == field.SQLName })
			if dbColumn == nil {
				// Column does not exist in DB, create it
				message := []string{"Creating column", field.SQLName, "with type",
					field.SQLType, "in table", generatedTable.Name}
				nullPart := ""
				defaultPart := ""
				if value, exists := field.Field.Tag.Lookup("pg"); exists {
					if strings.Contains(value, ",notnull") {
						nullPart = "not null"
						message = append(message, "(not null)")
					} else {
						message = append(message, "(nullable)")
					}
					if strings.Contains(value, "default:") {
						// Extract the part after default:
						defaultPart = "default " + string(regexp.MustCompile(".*default:(.*?)(?:,|$)").
							FindSubmatch([]byte(value))[1])
						message = append(message, "(with", defaultPart+")")
					}
				}
				log.Println(strings.Join(message, " "))
				if _, err := db.PgDB.Exec(fmt.Sprintf(`alter table "%s" add column "%s" %s %s %s`,
					pgTableName, field.SQLName, field.SQLType, nullPart, defaultPart)); err != nil {
					if strings.HasPrefix(err.Error(), "ERROR #23502 ") {
						// Column contains null values. We need a default.
						return errors.New(fmt.Sprintf(
							"Column %[3]s is to be created but it can't be filled with null %[1]s %[2]s (%[4]s)",
							"values because you specified 'notnull' in the field tag. ",
							"Please specify a default value, remove 'notnull' or drop all rows from the table",
							field.SQLName,
							err.Error(),
						))
					}
					return err
				}
			}
		}
		if options.RemoveOrphanedColumns {
			for _, column := range columns {
				field := findOrmField(generatedTable.Fields, func(f *orm.Field) bool {
					return f.SQLName == column.ColumnName
				})
				if field == nil {
					log.Println("Removing column", column.ColumnName, "with type",
						column.DataType, "in table", column.TableNameStr)
					if _, err := db.PgDB.Exec(fmt.Sprintf("alter table %s drop column %s",
						generatedTable.Name, column.ColumnName)); err != nil {
						return err
					}
				}
			}
		}
	}
	return nil
}

func (vs columnSlice) find(f func(column) bool) *column {
	for _, v := range vs {
		if f(v) {
			return &v
		}
	}
	return nil
}

func findOrmField(vs []*orm.Field, f func(*orm.Field) bool) *orm.Field {
	for _, v := range vs {
		if f(v) {
			return v
		}
	}
	return nil
}
