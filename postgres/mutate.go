package postgres

import (
	"strings"

	"github.com/go-pg/pg/v9"
	"github.com/go-pg/pg/v9/orm"
)

type tableCreator interface {
	CreateTable(model interface{}, options *orm.CreateTableOptions) error
}

func (db Database) CreateCollectionFromModel(name string, model interface{}, ifNotExists bool) error {
	return createCollectionFromModel(ifNotExists, name, db.PgDB, db.PgDB, model)
}

func createCollectionFromModel(ifNotExists bool, name string, db orm.DB, likeDb tableCreator, model interface{}) error {
	opts := &orm.CreateTableOptions{
		IfNotExists: ifNotExists,
	}
	if len(strings.TrimSpace(name)) == 0 {
		return likeDb.CreateTable(model, opts)
	} else {
		return tableModelQuery(
			db, name, model, false).CreateTable(opts)
	}
}

func (db Database) Insert(cn string, model interface{}) error {
	_, err := db.tableModelQuery(cn, model, false).Insert(model)
	return err
}

func (db Database) Update(cn string, model interface{}) error {
	_, err := db.tableModelQuery(cn, model, false).WherePK().Update(model)
	return err
}

func (db Database) Delete(cn string, model interface{}) error {
	_, err := db.tableModelQuery(cn, model, false).WherePK().Delete(model)
	return err
}

type TxMutationAdapter struct {
	tx *pg.Tx
}

func (TxMA TxMutationAdapter) CreateCollectionFromModel(name string, model interface{}, ifNotExists bool) error {
	return createCollectionFromModel(ifNotExists, name, TxMA.tx, TxMA.tx, model)
}

func (TxMA TxMutationAdapter) Insert(cn string, obj interface{}) error {
	_, err := tableModelQuery(TxMA.tx, cn, obj, false).Insert(obj)
	return err
}

func (TxMA TxMutationAdapter) Update(cn string, obj interface{}) error {
	_, err := tableModelQuery(TxMA.tx, cn, obj, false).WherePK().Update(obj)
	return err
}

func (TxMA TxMutationAdapter) Delete(cn string, obj interface{}) error {
	_, err := tableModelQuery(TxMA.tx, cn, obj, false).WherePK().Delete(obj)
	return err
}
