// Package register is responsible for
// registering this database system
package register

import (
	"github.com/go-pg/pg/v9"
	"gitlab.com/xdevs23/goqlorm/database/dbdefs"
	"gitlab.com/xdevs23/goqlorm/database/dbsystem"
	"gitlab.com/xdevs23/goqlorm/postgres"
)

// Register the database system
func init() {
	dbsystem.Register("PostgreSQL", func(config *dbsystem.DBAdapterConfig) dbdefs.Adapter {
		pgDB := pg.Connect(&pg.Options{
			User:     config.DBUser,
			Password: config.DBPassword,
			Database: config.DBName,
			Addr:     config.FinalConnectionString,
		})
		return &postgres.Database{PgDB: pgDB}
	})
}
