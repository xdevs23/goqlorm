// Package postgres provides an adapter for the database
// abstraction layer so that you can access PostgreSQL databases
package postgres
