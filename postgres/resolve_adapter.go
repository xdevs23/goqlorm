package postgres

import (
	"errors"
	"reflect"

	"gitlab.com/xdevs23/goqlorm/xstrings"

	"github.com/joomcode/errorx"

	"github.com/graphql-go/graphql"
)

func (db Database) DataFromDatabaseUsingResolveParams(
	objArr interface{},
	modelType reflect.Type,
	p graphql.ResolveParams) error {

	q := db.PgDB.Model(objArr)
	for i := 0; i < modelType.NumField(); i++ {
		field := modelType.Field(i)
		fieldNameLowerCase := xstrings.ToCamelCase(field.Name)
		if value := p.Args[fieldNameLowerCase]; value != nil {
			switch field.Type.Kind() {
			case
				reflect.Array,
				reflect.Slice:
				q.WhereIn(fieldNameLowerCase+" @> [?]", value)
			case
				reflect.Complex128,
				reflect.Complex64,
				reflect.Float64,
				reflect.Float32,
				reflect.Bool,
				reflect.Int8,
				reflect.Uint,
				reflect.Uint8,
				reflect.Uint16,
				reflect.Uint64,
				reflect.Uint32,
				reflect.Int16,
				reflect.Int64,
				reflect.Int32,
				reflect.Int,
				reflect.String:
				q.Where(fieldNameLowerCase+" = ?", value)
			case reflect.Map:
				return errorx.EnsureStackTrace(errors.New("maps are not supported yet"))
			}
		}
	}
	if err := q.Select(); err != nil {
		return err
	}
	return nil
}
