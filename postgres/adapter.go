package postgres

import (
	"github.com/go-pg/pg/v9"
)

type Database struct {
	PgDB *pg.DB
}
