package postgres

import (
	"fmt"
	"reflect"

	"gitlab.com/xdevs23/goqlorm/database/dbdefs"

	"github.com/go-pg/pg/v9"
	"github.com/go-pg/pg/v9/orm"
	"github.com/stoewer/go-strcase"
	dynamicstruct "github.com/xdevs23/dynamic-struct"
	"gitlab.com/xdevs23/goqlorm/reflectutil"
)

func (db Database) Close() error {
	return db.PgDB.Close()
}

func (db Database) IsConnected() bool {
	_, err := db.PgDB.Exec("select 1")
	return err == nil
}

func (db Database) RunInTransaction(fn dbdefs.InnerTransactionFunc) error {
	return db.PgDB.RunInTransaction(func(tx *pg.Tx) error {
		return fn(TxMutationAdapter{tx})
	})
}

func (db *Database) tableModelQuery(collectionName string, model interface{}, isSelect bool) *orm.Query {
	return tableModelQuery(db.PgDB, collectionName, model, isSelect)
}

func tableModelQueryReturnModified(dbOrTx orm.DB, collectionName string, model interface{}) (
	*orm.Query, interface{}) {

	modelToUse := model
	modelValue := reflect.ValueOf(model)
	modelType := reflectutil.PurifyType(reflect.TypeOf(model))
	pkgPath := modelType.PkgPath()
	if len(pkgPath) == 0 {
		// Use our package path instead
		pkgPath = reflect.TypeOf((*Database)(nil)).Elem().PkgPath()
	}

	// Here we are forcing pg to use our table name
	modifiedStruct := dynamicstruct.ExtendStruct(
		reflectutil.NewFromType(reflectutil.PurifyToType(model))).AddField("tableName", struct{}{},
		fmt.Sprintf(`pg:"%ss"`, ToPgString(collectionName)))
	builtModifiedStruct := modifiedStruct.BuildWithPkgPath(pkgPath)

	if model != nil {
		modelTypeUnwrapped := reflectutil.UnwrapType(reflect.TypeOf(model))
		unwrappedValue := reflectutil.UnwrapValue(modelValue)
		if modelTypeUnwrapped.Kind() == reflect.Slice {
			newSlice := reflectutil.NewSliceWithSize(builtModifiedStruct.New(), unwrappedValue.Len(), unwrappedValue.Len())
			newSliceVal := reflect.ValueOf(newSlice)
			for i := 0; i < unwrappedValue.Len(); i++ {
				entry := unwrappedValue.Index(i)
				newModel := builtModifiedStruct.New()
				reflectutil.CopyValueToModel(modelType, entry, newModel)
				newSliceVal.Index(i).Set(reflect.ValueOf(newModel).Elem())
			}
			modelToUse = newSlice
		} else {
			newModel := builtModifiedStruct.New()
			reflectutil.CopyValueToModel(modelType, unwrappedValue, newModel)
			modelToUse = newModel
		}
	}
	modelToUse = reflectutil.EnsureSinglePointer(modelToUse)
	q := orm.NewQuery(dbOrTx, modelToUse)
	return q, modelToUse
}

func tableModelQuery(dbOrTx orm.DB, collectionName string, model interface{}, _ bool) *orm.Query {
	q, _ := tableModelQueryReturnModified(dbOrTx, collectionName, model)
	return q
}

func ToPgString(str string) string {
	return strcase.SnakeCase(str)
}

func ToPgTableName(str string) string {
	return ToPgString(str) + "s"
}
