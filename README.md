# Go + GraphQL + Database (ORM)

[![GoDoc](https://godoc.org/gitlab.com/xdevs23/goqlorm?status.svg)](https://godoc.org/gitlab.com/xdevs23/goqlorm)
[![Go Report Card](https://goreportcard.com/badge/gitlab.com/xdevs23/goqlorm)](https://goreportcard.com/report/gitlab.com/xdevs23/goqlorm)

GraphQL API generator + Database ORM written in Go

**Note:** This is **BETA software** and is subject to breaking changes at any time. This includes anything
from simple refactors and package renames/split-ups to a complete project name change.

Creates the schema for you, sets up a GraphQL API and
handles everything from getting one or many entries,
creating, updating and deleting.

This makes everything really easy to use and provides
a quick way to get started while still giving you control over
everything.

If you want to know how to get started, take a brief look at
the [main.go](https://gitlab.com/xdevs23/goqlorm/-/blob/master/main.go) file until the documentation
is completed.

### Environment variables

| Variable name    | Description                                        |
|------------------|----------------------------------------------------|
| `LISTEN_ADDR`    | Address to bind the HTTP server to. Default: :8080 |
| `DB_ADDR`        | Database connection string. Default is provided by you (see [main.go](https://gitlab.com/xdevs23/goqlorm/-/blob/master/main.go)) | 
