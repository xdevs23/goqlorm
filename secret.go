package main

// Secret is a secret
type Secret struct {
	Name      string
	Timestamp int64
}
